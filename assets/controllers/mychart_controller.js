
import { Controller } from '@hotwired/stimulus';
import 'moment';
import 'chartjs-adapter-moment';

export default class extends Controller {
    connect() {
        this.element.addEventListener('chartjs:pre-connect', this._onPreConnect);
        this.element.addEventListener('chartjs:connect', this._onConnect);
    }

    disconnect() {
        // You should always remove listeners when the controller is disconnected to avoid side effects
        this.element.removeEventListener('chartjs:pre-connect', this._onPreConnect);
        this.element.removeEventListener('chartjs:connect', this._onConnect);
    }

    _onPreConnect(event) {
        // For instance you can format Y axis
        event.detail.options.scales = {
            yAxes: [
                {
                    ticks: {
                        callback: function (value, index, values) {
                            /* ... */
                        },
                    },
                },
            ],
        };
    }

    _onConnect(event) {
        // For instance you can listen to additional events
        event.detail.chart.options.onHover = (mouseEvent) => {
            /* ... */
        };
        event.detail.chart.options.onClick = (mouseEvent) => {
            /* ... */
        };
    }
}