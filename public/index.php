<?php
ini_set("error_log", __DIR__."/../var/log/php-error.log");

use App\Kernel;

require_once dirname(__DIR__).'/vendor/autoload_runtime.php';

return function (array $context) {

    if (in_array(@$context['REMOTE_ADDR'], ['93.105.223.55','127.0.0.1', '::1'], true)) {
        $context['APP_ENV'] = 'dev';
        $context['APP_DEBUG'] = 1;
    }
    return new Kernel($context['APP_ENV'], (bool) $context['APP_DEBUG']);
};
