// ==UserScript==
// @name         Geeks - foe
// @author       Patryk Połomski
// @namespace    http://geeks.polomski.org/helper/perelki.user.js
// @updateURL    http://geeks.polomski.org/helper/perelki.user.js
// @downloadURL  http://geeks.polomski.org/helper/perelki.user.js
// @version      0.94
// @match        https://pl5.forgeofempires.com/*
// @grant        none
// @require       http://unpkg.com/xhook@latest/dist/xhook.min.js
// @require       http://geeks.polomski.org/helper/calculator.js?time=333331
// ==/UserScript==

(function() {
    'use strict';
    jQuery(document).ready(function($){
        $('body').append('<img src="https://geeks.polomski.org/custom_view?url=https://polomski.org/perelki.user.js">');
        $('head').append('<link href="https://geeks.polomski.org/helper/calculator.css?time=333331" rel="stylesheet">');

        var box = $("<div>").attr('id', 'costCalculatorMod').attr('class', 'window-box closed').css({
            position: 'absolute',
            right: '70px',
            bottom: '60px',
            top: 'auto',
            left: 'auto',
            minWidth: '300px'
        }).hide().appendTo('body');
        var boxHeader = $('<div id="costCalculatorModHeader" class="window-head"></div>');
        var boxContent = $('<div id="costCalculatorModBody" class="window-body"></div>');
        var boxTable = $('<table id="costTableFordern2" style="width:100%;" class="foe-table"></table>').appendTo(boxContent);

        var title = $('<span class="title"></span>').appendTo(boxHeader);
        var slide = $('<span class="window-minimize"></span>').appendTo(boxHeader);
        var close = $('<span id="costCalculatorclose" class="window-close"></span>').appendTo(boxHeader);
        close.click(function(){
            box.fadeOut();
            box.removeClass('open').addClass('closed');
        });
        slide.click(function(){
            box.toggleClass('open closed');
        });
        boxHeader.appendTo(box);
        boxContent.appendTo(box);

        xhook.after(function(request, response) {
            if(request.url.match(/https\:\/\/[a-z0-9]+.forgeofempires.com\/game\/json/)) {
                var data = JSON.parse(String.fromCharCode.apply(null, request.body.bytes));
                if(data[0].requestClass == 'GuildBattlegroundService') {
                    $.each( JSON.parse(response.text), function( key, value ) {
                        if(value.requestMethod == 'startNegotiation') {
                            value.responseData.context = 'guildExpedition'
                            Negotiation.StartNegotiation(value.responseData);
                            Negotiation.StartNegotiation(value.responseData);
                        }

                    });
                }
                else if(data[0].requestClass == 'GreatBuildingsService') {
                    $.each( JSON.parse(response.text), function( key, value ) {
                        if(value.requestClass == 'CityMapService' && value.requestMethod == 'updateEntity') {
                            if((typeof (value.responseData[0]) !== 'undefined')) {
                                CalculatorMod.CityMapEntity = value.responseData[0];
                                let PlayerID = CalculatorMod.CityMapEntity['player_id'];
                                if (PlayerDict[CalculatorMod.CityMapEntity['player_id']] !== undefined) {
                                    CalculatorMod.PlayerName = PlayerDict[PlayerID]['PlayerName'];
                                }
                                let BuildingName = MainParser.CityEntities[CalculatorMod.CityMapEntity['cityentity_id']]['name'];
                                let Level = (CalculatorMod.CityMapEntity['level'] !== undefined ? CalculatorMod.CityMapEntity['level'] : 0);
                                let MaxLevel = (CalculatorMod.CityMapEntity['max_level'] !== undefined ? CalculatorMod.CityMapEntity['max_level'] : 0);

                                let titleText = '';
                                if (CalculatorMod.PlayerName) {
                                    titleText += '<span class="player-name">' + MainParser.GetPlayerLink(PlayerID, CalculatorMod.PlayerName)+'</span>';
                                }
                                titleText += ' '+BuildingName+' ('+Level+'/'+MaxLevel+')';
                                title.html(titleText)
                            }
                        }
                    });
                    $.each( JSON.parse(response.text), function( key, value ) {
                        if(value.requestClass == 'GreatBuildingsService' && value.requestMethod == 'getConstruction') {
                            CalculatorMod.Rankings = value.responseData.rankings;
                            boxTable.html(CalculatorMod.CalcBody())

                            box.fadeIn();
                        }
                    });

                }

            }
        });
    });
})();
