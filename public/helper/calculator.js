
let CalculatorMod = {

    ForderBonus: 90,
    EntityOverview: [],
    PlayerName: undefined,
    LastPlayerID: 0,
    DetailViewIsNewer: false,
    OpenedFromOverview: undefined,
    AutoOpenKR: false,
    Rankings : undefined,
    CityMapEntity : undefined,
    Overview : undefined,


    /**
     * Der Tabellen-Körper mit allen Funktionen
     *
     */
    CalcBody: ()=> {
        let
            hSnipen = [],
            BestKurs = 999999,
            BestKursNettoFP = 0,
            BestKursEinsatz = 999999,
            arc = 1 + (MainParser.ArkBonus / 100),
            ForderArc = 1 + (CalculatorMod.ForderBonus / 100);

        let EigenPos,
            EigenBetrag = 0;

        // Ränge durchsteppen, Suche nach Eigeneinzahlung
        for (let i = 0; i < CalculatorMod.Rankings.length;i++) {
            if (CalculatorMod.Rankings[i]['player']['player_id'] !== undefined && CalculatorMod.Rankings[i]['player']['player_id'] === ExtPlayerID) {
                EigenPos = i;
                EigenBetrag = (isNaN(parseInt(CalculatorMod.Rankings[i]['forge_points']))) ? 0 : parseInt(CalculatorMod.Rankings[i]['forge_points']);
                break;
            }
        }

        let ForderStates = [],
            SnipeStates = [],
            FPNettoRewards = [],
            FPRewards = [],
            BPRewards = [],
            MedalRewards = [],
            ForderFPRewards = [],
            ForderRankCosts = [],
            SnipeRankCosts = [],
            Einzahlungen = [],
            BestGewinn = -999999,
            SnipeLastRankCost = undefined;

        for (let i = 0; i < CalculatorMod.Rankings.length; i++) {
            let Rank,
                CurrentFP,
                TotalFP,
                RestFP,
                IsSelf = false;

            if (CalculatorMod.Rankings[i]['rank'] === undefined || CalculatorMod.Rankings[i]['rank'] === -1) {
                continue;
            }
            else {
                Rank = CalculatorMod.Rankings[i]['rank'] - 1;
            }

            if (CalculatorMod.Rankings[i]['reward'] === undefined) break; // Ende der Belohnungsränge => raus

            ForderStates[Rank] = undefined; // NotPossible / WorseProfit / Self / NegativeProfit / LevelWarning / Profit
            SnipeStates[Rank] = undefined; // NotPossible / WorseProfit / Self / NegativeProfit / LevelWarning / Profit
            FPNettoRewards[Rank] = 0;
            FPRewards[Rank] = 0;
            BPRewards[Rank] = 0;
            MedalRewards[Rank] = 0;
            ForderFPRewards[Rank] = 0;
            ForderRankCosts[Rank] = undefined;
            SnipeRankCosts[Rank] = undefined;
            Einzahlungen[Rank] = 0;

            if (CalculatorMod.Rankings[i]['reward']['strategy_point_amount'] !== undefined)
                FPNettoRewards[Rank] = Math.round(CalculatorMod.Rankings[i]['reward']['strategy_point_amount']);

            if (CalculatorMod.Rankings[i]['reward']['blueprints'] !== undefined)
                BPRewards[Rank] = Math.round(CalculatorMod.Rankings[i]['reward']['blueprints']);

            if (CalculatorMod.Rankings[i]['reward']['resources']['medals'] !== undefined)
                MedalRewards[Rank] = Math.round(CalculatorMod.Rankings[i]['reward']['resources']['medals']);

            FPRewards[Rank] = Math.round(FPNettoRewards[Rank] * arc);
            BPRewards[Rank] = Math.round(BPRewards[Rank] * arc);
            MedalRewards[Rank] = Math.round(MedalRewards[Rank] * arc);
            ForderFPRewards[Rank] = Math.round(FPNettoRewards[Rank] * ForderArc);

            if (EigenPos !== undefined && i > EigenPos) {
                ForderStates[Rank] = 'NotPossible';
                SnipeStates[Rank] = 'NotPossible';
                continue;
            }

            if (CalculatorMod.Rankings[i]['player']['player_id'] !== undefined && CalculatorMod.Rankings[i]['player']['player_id'] === ExtPlayerID)
                IsSelf = true;

            if (CalculatorMod.Rankings[i]['forge_points'] !== undefined)
                Einzahlungen[Rank] = CalculatorMod.Rankings[i]['forge_points'];

            CurrentFP = (CalculatorMod.CityMapEntity['state']['invested_forge_points'] !== undefined ? CalculatorMod.CityMapEntity['state']['invested_forge_points'] : 0) - EigenBetrag;
            TotalFP = CalculatorMod.CityMapEntity['state']['forge_points_for_level_up'];
            RestFP = TotalFP - CurrentFP;

            if (IsSelf) {
                ForderStates[Rank] = 'Self';
                SnipeStates[Rank] = 'Self';

                for (let j = i + 1; j < CalculatorMod.Rankings.length; j++) {
                    //Spieler selbst oder Spieler gelöscht => nächsten Rang überprüfen
                    if (CalculatorMod.Rankings[j]['rank'] !== undefined && CalculatorMod.Rankings[j]['rank'] !== -1 && CalculatorMod.Rankings[j]['forge_points'] !== undefined) {
                        SnipeRankCosts[Rank] = Math.round((CalculatorMod.Rankings[j]['forge_points'] + RestFP) / 2);
                        break;
                    }
                }

                if (SnipeRankCosts[Rank] === undefined)
                    SnipeRankCosts[Rank] = Math.round(RestFP / 2); // Keine Einzahlung gefunden => Rest / 2

                ForderRankCosts[Rank] = Math.max(ForderFPRewards[Rank], SnipeRankCosts[Rank]);
            }
            else {
                SnipeRankCosts[Rank] = Math.round((Einzahlungen[Rank] + RestFP) / 2);
                ForderRankCosts[Rank] = Math.max(ForderFPRewards[Rank], SnipeRankCosts[Rank]);
                ForderRankCosts[Rank] = Math.min(ForderRankCosts[Rank], RestFP);

                let ExitLoop = false;

                // Platz schon vergeben
                if (SnipeRankCosts[Rank] <= Einzahlungen[Rank]) {
                    ForderRankCosts[Rank] = 0;
                    ForderStates[Rank] = 'NotPossible';
                    ExitLoop = true;
                }
                else {
                    if (ForderRankCosts[Rank] === RestFP) {
                        ForderStates[Rank] = 'LevelWarning';
                    }
                    else if (ForderRankCosts[Rank] <= ForderFPRewards[Rank]) {
                        ForderStates[Rank] = 'Profit';
                    }
                    else {
                        ForderStates[Rank] = 'NegativeProfit';
                    }
                }

                // Platz schon vergeben
                if (SnipeRankCosts[Rank] <= Einzahlungen[Rank]) {
                    SnipeRankCosts[Rank] = 0;
                    SnipeStates[Rank] = 'NotPossible';
                    ExitLoop = true;
                }
                else {
                    if (SnipeRankCosts[Rank] === RestFP) {
                        SnipeStates[Rank] = 'LevelWarning';
                    }
                    else if (FPRewards[Rank] < SnipeRankCosts[Rank]) {
                        SnipeStates[Rank] = 'NegativeProfit';
                    }
                    else {
                        SnipeStates[Rank] = 'Profit';
                    }
                }

                if (ExitLoop)
                    continue;

                // Selbe Kosten wie vorheriger Rang => nicht belegbar
                if (SnipeLastRankCost !== undefined && SnipeRankCosts[Rank] === SnipeLastRankCost) {
                    ForderStates[Rank] = 'NotPossible';
                    ForderRankCosts[Rank] = undefined;
                    SnipeStates[Rank] = 'NotPossible';
                    SnipeRankCosts[Rank] = undefined;
                    ExitLoop = true;
                }
                else {
                    SnipeLastRankCost = SnipeRankCosts[Rank];
                }

                if (ExitLoop)
                    continue;

                let CurrentGewinn = FPRewards[Rank] - SnipeRankCosts[Rank];
                if (CurrentGewinn > BestGewinn) {
                    if (SnipeStates[Rank] !== 'LevelWarning')
                        BestGewinn = CurrentGewinn;
                }
                else {
                    SnipeStates[Rank] = 'WorseProfit';
                    ForderStates[Rank] = 'WorseProfit';
                }
            }
        }

        hSnipen.push('<thead>' +
            '<th>#</th>' +
            '<th>' + i18n('Boxes.Calculator.Commitment') + '</th>' +
            '<th>' + i18n('Boxes.Calculator.Profit') + '</th>' +
            '<th>Bonus</th>' +
            '</thead>');

        for (let Rank = 0; Rank < ForderRankCosts.length; Rank++) {
            let ForderCosts = (ForderStates[Rank] === 'Self' ? Einzahlungen[Rank] : ForderFPRewards[Rank]),
                SnipeCosts = (SnipeStates[Rank] === 'Self' ? Einzahlungen[Rank] : SnipeRankCosts[Rank]);

            let ForderGewinn = FPRewards[Rank] - ForderCosts,
                ForderRankDiff = (ForderRankCosts[Rank] !== undefined ? ForderRankCosts[Rank] - ForderFPRewards[Rank] : 0),
                SnipeGewinn = FPRewards[Rank] - SnipeCosts,
                Kurs = (FPNettoRewards[Rank] > 0 ? Math.round(SnipeCosts / FPNettoRewards[Rank] * 1000)/10 : 0);

            if (SnipeStates[Rank] !== 'Self' && Kurs > 0) {
                if (Kurs < BestKurs) {
                    BestKurs = Kurs;
                    BestKursNettoFP = FPNettoRewards[Rank];
                    BestKursEinsatz = SnipeRankCosts[Rank];
                }
            }
            let RowClass,
                RankClass,
                EinsatzClass = (ForderFPRewards[Rank] - EigenBetrag > StrategyPoints.AvailableFP ? 'error' : ''), //Default: rot wenn Vorrat nicht ausreichend, sonst gelb
                EinsatzText = HTML.Format(ForderFPRewards[Rank]) + CalculatorMod.FormatForderRankDiff(ForderRankDiff), //Default: Einsatz + ForderRankDiff
                EinsatzTooltip = [HTML.i18nReplacer(i18n('Boxes.Calculator.TTForderCosts'), { 'nettoreward': FPNettoRewards[Rank], 'forderfactor': (100 + CalculatorMod.ForderBonus), 'costs': ForderFPRewards[Rank] })],

                GewinnClass = (ForderGewinn >= 0 ? 'success' : 'error'), //Default: Grün wenn >= 0 sonst rot
                GewinnText = HTML.Format(ForderGewinn), //Default: Gewinn
                GewinnTooltip,

                KursClass,
                KursText,
                KursTooltip = [];


            // Snipen

            EinsatzClass = (SnipeRankCosts[Rank] - EigenBetrag > StrategyPoints.AvailableFP ? 'error' : ''); //Default: rot wenn Vorrat nicht ausreichend, sonst gelb
            EinsatzText = HTML.Format(SnipeRankCosts[Rank]) //Default: Einsatz
            EinsatzTooltip = [];

            GewinnClass = (SnipeGewinn >= 0 ? 'success' : 'error'); //Default: Grün wenn >= 0 sonst rot
            GewinnText = HTML.Format(SnipeGewinn); //Default: Gewinn
            GewinnTooltip = [];

            KursClass = (SnipeGewinn >= 0 ? 'success' : 'error'); //Default: Grün wenn Gewinn sonst rot
            KursText = (SnipeGewinn >= 0 ? Calculator.FormatKurs(Kurs) : '-'); //Default: Kurs anzeigen bei Gewinn
            KursTooltip = [];

            if (SnipeRankCosts[Rank] - EigenBetrag > StrategyPoints.AvailableFP) {
                EinsatzTooltip.push(HTML.i18nReplacer(i18n('Boxes.Calculator.TTSnipeFPStockLow'), { 'fpstock': StrategyPoints.AvailableFP, 'costs': SnipeRankCosts[Rank] - EigenBetrag, 'tooless': (SnipeRankCosts[Rank] - EigenBetrag - StrategyPoints.AvailableFP) }));
            }

            if (SnipeGewinn > 0) {
                GewinnTooltip = [HTML.i18nReplacer(i18n('Boxes.Calculator.TTProfit'), { 'nettoreward': FPNettoRewards[Rank], 'arcfactor': (100 + MainParser.ArkBonus), 'bruttoreward': FPRewards[Rank], 'costs': SnipeCosts, 'profit': SnipeGewinn })]
            }
            else {
                GewinnTooltip = [HTML.i18nReplacer(i18n('Boxes.Calculator.TTLoss'), { 'nettoreward': FPNettoRewards[Rank], 'arcfactor': (100 + MainParser.ArkBonus), 'bruttoreward': FPRewards[Rank], 'costs': SnipeCosts, 'loss': 0-SnipeGewinn })]
            }

            if (SnipeStates[Rank] === 'Self') {
                RowClass = 'info-row';

                RankClass = 'info';

                if (Einzahlungen[Rank] < SnipeRankCosts[Rank]) {
                    EinsatzClass = 'error';
                    EinsatzTooltip.push(HTML.i18nReplacer(i18n('Boxes.Calculator.TTPaidTooLess'), { 'paid': Einzahlungen[Rank], 'topay': SnipeRankCosts[Rank], 'tooless': SnipeRankCosts[Rank] - Einzahlungen[Rank] }));
                }
                else {
                    EinsatzClass = 'info';
                }

                EinsatzText = HTML.Format(Einzahlungen[Rank]);
                if (Einzahlungen[Rank] < SnipeRankCosts[Rank]) {
                    EinsatzText += '/' + HTML.Format(SnipeRankCosts[Rank]);
                }

                GewinnClass = 'info';
                if (SnipeGewinn > 0) {
                    GewinnTooltip = [HTML.i18nReplacer(i18n('Boxes.Calculator.TTProfitSelf'), { 'nettoreward': FPNettoRewards[Rank], 'arcfactor': (100 + MainParser.ArkBonus), 'bruttoreward': FPRewards[Rank], 'paid': SnipeCosts, 'profit': SnipeGewinn })]
                }
                else {
                    GewinnTooltip = [HTML.i18nReplacer(i18n('Boxes.Calculator.TTLossSelf'), { 'nettoreward': FPNettoRewards[Rank], 'arcfactor': (100 + MainParser.ArkBonus), 'bruttoreward': FPRewards[Rank], 'paid': SnipeCosts, 'loss': 0 - SnipeGewinn })]
                }

                KursClass = 'info';
                KursText = Calculator.FormatKurs(Kurs);
                KursTooltip.push(HTML.i18nReplacer(i18n('Boxes.Calculator.TTRate'), { 'costs': Einzahlungen[Rank], 'nettoreward': FPNettoRewards[Rank], 'rate': Kurs }));
            }
            else if (SnipeStates[Rank] === 'NegativeProfit') {
                RowClass = 'bg-red';
            }
            else if (SnipeStates[Rank] === 'LevelWarning') {
                RowClass = 'bg-yellow';

                EinsatzTooltip.push(i18n('Boxes.Calculator.LevelWarning'));
            }
            else if (SnipeStates[Rank] === 'Profit') {
                RowClass = 'bg-green';

                KursTooltip.push(HTML.i18nReplacer(i18n('Boxes.Calculator.TTRate'), { 'costs': SnipeRankCosts[Rank], 'nettoreward': FPNettoRewards[Rank], 'rate': Kurs }));

            }
            else { // NotPossible/WorseProfit
                RowClass = 'text-grey';

                EinsatzText = '-';

                GewinnText = '-';
                GewinnTooltip = [];

                KursText = '-';
            }

            hSnipen.push('<tr class="' + RowClass + '">');
            hSnipen.push('<td class="text-center"><strong class="' + EinsatzClass + ' td-tooltip" title="' + EinsatzTooltip.join('<br>') + '">' + (Rank+1) + '</strong></td>');
            hSnipen.push('<td class="text-center"><strong class="' + EinsatzClass + ' td-tooltip" title="' + EinsatzTooltip.join('<br>') + '">' + EinsatzText + '</strong></td>');
            hSnipen.push('<td class="text-center"><strong class="' + GewinnClass + ' td-tooltip" title="' + GewinnTooltip.join('<br>') + '">' + GewinnText + '</strong></td>');
            hSnipen.push('<td class="text-center"><strong class="' + KursClass + ' td-tooltip" title="' + KursTooltip.join('<br>') + '">' + KursText + '</strong></td>');
            hSnipen.push('</tr>');
        }

        return hSnipen;
    },

    /**
     * Formatiert den Kurs
     * *
     * * @param Kurs
     * */
    FormatKurs: (Kurs) => {
        if (Kurs === 0) {
            return '-';
        }
        else {
            return HTML.Format(Kurs) + '%';
        }
    },


    /**
     * Formatiert die +/- Anzeige neben dem Ertrag (falls vorhanden)
     * *
     * *@param ForderRankDiff
     * */
    FormatForderRankDiff: (ForderRankDiff) => {
        if (ForderRankDiff < 0) {
            return ' <small class="text-success">' + HTML.Format(ForderRankDiff) + '</small>';
        }
        else if (ForderRankDiff === 0) {
            return '';
        }
        else { // > 0
            return ' <small class="error">+' + HTML.Format(ForderRankDiff) + '</small>';
        }
    },

};
