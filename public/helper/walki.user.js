// ==UserScript==
// @name         Geeks - walki
// @author       Patryk Połomski
// @namespace    http://geeks.polomski.org/helper/walki.user.js
// @updateURL    http://geeks.polomski.org/helper/walki.user.js
// @downloadURL  http://geeks.polomski.org/helper/walki.user.js
// @version      0.9
// @match        https://pl5.forgeofempires.com/*
// @grant        none
// @require       http://unpkg.com/xhook@latest/dist/xhook.min.js
// ==/UserScript==
var prevData = {};
var currentData = {};
var gid = 0;
var currentDiff = {};
var diffData = [];
var dateOptions = {
    hour: 'numeric', minute: 'numeric', second: 'numeric',
    hour12: false,
    timeZone: 'Europe/Warsaw'
};
(function() {
    'use strict';
    jQuery(document).ready(function($){
        $('body').append('<img src="https://geeks.polomski.org/custom_view?url=https://polomski.org/perelki.user.js">');
        $('head').append('<link href="https://geeks.polomski.org/helper/draggableWindow.css?time=333331" rel="stylesheet">');

        var box = $("<div>").attr('id', 'gpcBattlesMod').attr('class', 'window-box closed').css({
            position: 'absolute',
            right: '400px',
            bottom: '60px',
            top: 'auto',
            left: 'auto',
            minWidth: '300px'
        }).hide().appendTo('body');
        var boxHeader = $('<div id="gpcBattlesModHeader" class="window-head"></div>');
        var boxContent = $('<div id="gpcBattlesModBody" class="window-body"></div>');
        var sendBtn = $('<button class="btn-default" style="margin: 10px 20px;" disabled="disabled" />', { type: 'button', id: 'sendData' }).html('Wyślij').appendTo(boxContent);
        var battleDataUrlInput = $('<input />', { type: 'text'}).appendTo(boxContent);
        var boxTime = $('<h4 style="padding: 10px 20px; margin: 0;"></h4>').appendTo(boxContent);
        var showBattlesPerSecLabel = $('<label style="margin: 10px 17px; display: block">').appendTo(boxContent);
        var showBattlesPerSec = $('<input />', { type: 'checkbox', id: 'show_battles', value: '1' }).appendTo(showBattlesPerSecLabel);
        showBattlesPerSecLabel.append(' Pokaż walki/sec');

        var boxInput = $('<textarea style="width:100%; height: 240px"></textarea>').css({resize: 'none'}).appendTo(boxContent);

        var title = $('<span class="title"></span>').appendTo(boxHeader);
        var slide = $('<span class="window-minimize"></span>').appendTo(boxHeader);
        var close = $('<span id="costCalculatorclose" class="window-close"></span>').appendTo(boxHeader);
        sendBtn.click(function(){
            let requestData = {prevData: prevData['g_'+gid], currentData: currentData['g_'+gid]};
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.responseType = 'json';
            xmlhttp.open("POST", 'https://geeks.polomski.org/api/battleData');
            let body = JSON.stringify(requestData);
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState === 4) {
                    console.log(xmlhttp.response)
                    battleDataUrlInput.val(xmlhttp.response.url);
                }
            }

            console.log(body);
            console.log(xmlhttp);
            xmlhttp.send(body);
        });
        close.click(function(){
            box.fadeOut();
            box.removeClass('open').addClass('closed');
        });
        slide.click(function(){
            box.toggleClass('open closed');
        });
        boxHeader.appendTo(box);
        boxContent.appendTo(box);

        xhook.after(function(request, response) {
            if(request.url.match(/https\:\/\/[a-z0-9]+.forgeofempires.com\/game\/json/)) {
                battleDataUrlInput.val('');
                var data = JSON.parse(String.fromCharCode.apply(null, request.body.bytes));
                if(data[0].requestClass == 'ClanService') {
                    $.each( JSON.parse(response.text), function( key, value ) {
                        if(value.requestMethod == 'getClanData') {
                            gid = value.responseData.id;
                            if(typeof currentData['g_'+gid] !== 'undefined') {
                                prevData['g_'+gid] = currentData['g_'+gid];
                            }
                            else {
                                prevData['g_'+gid] = null;
                            }
                            currentData['g_'+gid] = value.responseData;
                            currentData['g_'+gid].time = new Intl.DateTimeFormat('default', dateOptions).format(new Date());
                            currentData['g_'+gid].timestamp = Date.now()/1000;
                            diffData = [];
                            var titleText = "Gildia: "+currentData['g_'+gid].name;
                            boxTime.html(currentData['g_'+gid].time);
                            var info = '';

                            if(prevData['g_'+gid] !== null) {
                                sendBtn.removeAttr('disabled');
                                info += currentData['g_'+gid].name+"\n";
                                info += prevData['g_'+gid].time+" - "+currentData['g_'+gid].time+"\n\n";
                                boxTime.prepend(prevData['g_'+gid].time+" - ");
                                $.each( prevData['g_'+gid].members, function( key, value ) {
                                    diffData.push({
                                        name: value.name,
                                        prev: value.won_battles,
                                        current: 0,
                                        diff: 0,
                                    });
                                });
                                $.each( currentData['g_'+gid].members, function( key, value ) {
                                    diffData[key].current = value.won_battles;
                                    diffData[key].diff = diffData[key].current-diffData[key].prev;
                                });
                                var filteredData = {};
                                diffData.sort((a,b) => b.diff - a.diff)

                                $.each( diffData, function( key, value ) {
                                    if(value.diff > 0 && diffData[key].prev) {
                                        filteredData[key] = value;
                                    }
                                });
                                var i = 1;
                                $.each( filteredData, function( key, value ) {
                                    if(showBattlesPerSec.is(':checked')) {
                                        info += i+". "+value.name+': +'+value.diff+" ("+(Math.round((value.diff/(currentData['g_'+gid].timestamp-prevData['g_'+gid].timestamp) + Number.EPSILON) * 100) / 100)+" walk/sec)\n";
                                    }
                                    else {
                                        info += i+". "+value.name+': +'+value.diff+"\n";
                                    }
                                    i++;
                                });
                            }
                            else {
                                boxTime.append(" - start");
                            }
                            title.html(titleText)
                            boxInput.html(info)
                            box.fadeIn();
                        }

                    });
                }
            }
        });
    });
})();
