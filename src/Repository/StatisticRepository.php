<?php

namespace App\Repository;

use App\Entity\Player;
use App\Entity\Statistic;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Statistic|null find($id, $lockMode = null, $lockVersion = null)
 * @method Statistic|null findOneBy(array $criteria, array $orderBy = null)
 * @method Statistic[]    findAll()
 * @method Statistic[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatisticRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Statistic::class);
    }
    public function applyFilters(QueryBuilder $qb, array $criteria): void {
        if(isset($criteria['dateFrom'])) {
            $from = clone $criteria['dateFrom'];
            $qb->andWhere($qb->expr()->gte('rd.createdAt', ':dateFrom'))
                ->setParameter('dateFrom', $from)
            ;
        }
        if(isset($criteria['dateTo']) && $criteria['dateTo'] instanceof \DateTimeInterface) {
            $to = clone $criteria['dateTo'];
            $to->add(new \DateInterval('P1D'));
            $qb->andWhere($qb->expr()->lt('rd.createdAt', ':dateTo'))
                ->setParameter('dateTo', $to)
            ;
        }
    }


    public function getBy(?array $criteria = [], ?Player $player = null)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.rawData', 'rd')
            ->orderBy('rd.createdAt', 'ASC');

        $this->applyFilters($qb, $criteria);

        if($player) {
            $qb->andWhere($qb->expr()->eq('e.player', ':player'))
                ->setParameter('player', $player);
        }
        return $qb->getQuery()->getResult();
    }
//    /**
//     * @return Statistic[] Returns an array of Statistic objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Statistic
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
