<?php

namespace App\Repository;

use App\Entity\AdventureStatistic;
use App\Entity\Era;
use App\Entity\GuildTreasury;
use App\Entity\Player;
use App\Entity\RawData;
use App\Entity\Resource;
use App\Handler\RawDataHandler;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GuildTreasury|null find($id, $lockMode = null, $lockVersion = null)
 * @method GuildTreasury|null findOneBy(array $criteria, array $orderBy = null)
 * @method GuildTreasury[]    findAll()
 * @method GuildTreasury[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GuildTreasuryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GuildTreasury::class);
    }
    public function applyFilters(QueryBuilder $qb, array $criteria): void {
        if(isset($criteria['dateFrom'])) {
            $from = clone $criteria['dateFrom'];
            if(isset($criteria['gpc']) && $criteria['gpc']) {
                $from->setTime(8,0);
            }
            $qb->andWhere($qb->expr()->gte('e.createdAt', ':dateFrom'))
                ->setParameter('dateFrom', $from)
            ;
        }
        if(isset($criteria['dateTo']) && $criteria['dateTo'] instanceof \DateTimeInterface) {
            $to = clone $criteria['dateTo'];
            if(isset($criteria['gpc']) && $criteria['gpc']) {
                $to->setTime(8,0);
            }
            else {
                $to->add(new \DateInterval('P1D'));
            }
            $qb->andWhere($qb->expr()->lt('e.createdAt', ':dateTo'))
                ->setParameter('dateTo', $to)
            ;
        }
        if(isset($criteria['era']) && $criteria['era'] instanceof Era) {
            $qb->leftJoin('e.resource', 'rrr')
                ->andWhere($qb->expr()->eq('rrr.era', ':era'))
                ->setParameter('era', $criteria['era']);
        }
        if(isset($criteria['player']) && $criteria['player'] instanceof Player) {
            $qb->andWhere($qb->expr()->eq('e.player', ':player'))
                ->setParameter('player', $criteria['player']);
        }
        if(isset($criteria['action']) && $criteria['action']) {
            $qb->andWhere($qb->expr()->eq('e.action', ':action'))
                ->setParameter('action', $criteria['action']);
        }
    }

    public function listQb(?array $criteria = [], ?Player $player = null)
    {
        $qb = $this->createQueryBuilder('e');
        $this->applyFilters($qb, $criteria);
        $qb->orderBy('e.createdAt', 'DESC');
        return $qb;
    }

    public function groupedByAge(?array $criteria = [], ?Player $player = null)
    {
        $qb = $this->createQueryBuilder('e');
        $qb
            ->select('SUM(e.amount) as amount', 'er.number as era', 'er.color', 'e.action', 'count(e.action) as aa, r.name, r.id as rid')
            ->leftJoin('e.resource', 'r')
            ->leftJoin('r.era', 'er')
            ->where($qb->expr()->notIn('r.slug', ['guild_expedition_point','guild_championship_trophy_gold','guild_championship_trophy_silver','guild_championship_trophy_bronze']))
            ->groupBy('e.resource')
            ->addGroupBy('e.action')
            ->orderBy('r.era', 'ASC');

        $this->applyFilters($qb, $criteria);

        if($player) {
            $qb->andWhere($qb->expr()->eq('e.player', ':player'))
                ->setParameter('player', $player);
        }
        $data = [];

        foreach ($qb->getQuery()->getArrayResult() as $item) {
            $age = 'era.i'.$item['era'];
            if(!isset($data[$age])) {
                $data[$age] = [
                    'resources' => [
                        'sum' => []
                    ],
                ];
            }
            if(!isset($data[$age][$item['action']])) {
                $data[$age][$item['action']] = $item;
            }
            else {
                $data[$age][$item['action']]['amount'] += $item['amount'];
            }
            if(!isset($data[$age]['resources'][$item['action']])) {
                $data[$age]['resources'][$item['action']] = [];
            }
            if(!isset($data[$age]['resources']['sum'][$item['rid']])) {
                $data[$age]['resources']['sum'][$item['rid']] = $item;
            }
            else {
                $data[$age]['resources']['sum'][$item['rid']]['amount'] += $item['amount'];
            }
            $data[$age]['resources'][$item['action']][] = $item;
            $data[$age]['color'] = RawDataHandler::hex2rgba($item['color'], 0.3);

        }
        return $data;
    }


    public function groupedByResource($actions, ?array $criteria = [], ?Player $player = null): array
    {
        $qb = $this->_em->getRepository(Resource::class)->createQueryBuilder('e');
        $qb
            ->leftJoin('e.era', 'er')
            ->where($qb->expr()->gt('er.number', 1))
            ->orderBy('e.era', 'ASC');
        /** @var Resource[] $resources */
        $resources = $qb->getQuery()->getResult();
        $dates = [];
        $series = [];
        foreach ($resources as $resource) {
            $series[$resource->getId()] = [
                'era' => $resource->getEra(),
                'bgColor' => $resource->getEra()->getRGBAColor(),
                'backgroundColor' => $resource->getEra()->getRGBAColor(),
                'borderColor' => $resource->getEra()->getRGBAColor(),
                'slug' => $resource->getSlug(),
                'label' => $resource->getName(),
                'fill' => false,
                'data' => [],
            ];
        }

        $qb = $this->createQueryBuilder('e');
        $qb
            ->select('SUM(e.amount) as amount', 'e.createdAt', 'r.id', 'r.slug')
            ->leftJoin('e.resource', 'r')
            ->where($qb->expr()->in('e.action', $actions))
            ->addGroupBy('r.id')
            ->addGroupBy('group_date')
            ->orderBy('e.createdAt', 'ASC')
            ->addOrderBy('r.era', 'ASC');

        $this->applyFilters($qb, $criteria);

        match ($criteria['groupBy']??'') {
            'month' => $qb->addSelect("date_format(e.createdAt,'%Y-%m') as group_date"),
            'week' => $qb->addSelect("date_format(e.createdAt,'%Y-%u') as group_date"),
            default => $qb->addSelect("date_format(e.createdAt,'%Y-%m-%d') as group_date"),
        };

        if($player) {
            $qb->andWhere($qb->expr()->eq('e.player', ':player'))
                ->setParameter('player', $player);
        }
        $tmpData = [];
        foreach ($qb->getQuery()->getArrayResult() as $item) {
            $date = $item['group_date'];
            if(!isset($tmpData[$date])) {
                $tmpData[$date] = ['item' => ['sum' => 0, 'createdAt' => $item['createdAt']], 'resources'=>[]];
            }
            $tmpData[$date]['resources'][$item['slug']] = $item;
            $tmpData[$date]['item']['sum'] += $item['amount'];
        }
        foreach ($tmpData as $tmpDatum) {
            $dates[] = $tmpDatum['item']['createdAt']->getTimestamp();
            foreach ($series as $k => $s) {
                $series[$k]['data'][$tmpDatum['item']['createdAt']->getTimestamp()] = 0;
            }
        }

        foreach ($tmpData as $tmpDatum) {
            /** @var AdventureStatistic $stat */
            foreach ($tmpDatum['resources'] as $resource) {
                if(isset($series[$resource['id']])) {
                    $series[$resource['id']]['data'][$tmpDatum['item']['createdAt']->getTimestamp()] += $tmpDatum['resources'][$resource['slug']]['amount'];
                }
            }
        }


        $series = array_values($series);

        foreach ($series as $k => $v) {
            if(!array_filter($v['data'])) {
                unset($series[$k]);
            }
            else {
                $series[$k]['data'] = array_values($v['data']);
            }
        }
        $series = array_values($series);
        $data = [
            'labels' => $dates,
            'datasets' => $series,
        ];
        return $data;
    }
    public function groupedByAgeCostGPC(?array $criteria = [])
    {
        $players = $this->_em->getRepository(Player::class)->findBy(['active' => true]);
        $eras = [];
        foreach($players as $player)
        {
            $eras['era.i'.$player->getEra()->getNumber()][] = $player;
        }
        $qb = $this->createQueryBuilder('e');
        $qb
            ->select('SUM(e.amount) as amount', 'er.number as era', 'er.color', 'e.action', 'count(e.action) as aa')
            ->leftJoin('e.resource', 'r')
            ->leftJoin('r.era', 'er')
            ->where($qb->expr()->in('e.action', [GuildTreasury::ACTION_GPC_BUILDING]))
            ->groupBy('r.era')
            ->orderBy('r.era', 'ASC');

        $this->applyFilters($qb, $criteria);

        $data = [];

        foreach ($qb->getQuery()->getArrayResult() as $item) {
            $age = 'era.i'.$item['era'];
            if(!isset($data[$age])) {
                $data[$age] = [];
            }
            $data[$age]['color'] = RawDataHandler::hex2rgba($item['color'], 0.3);
            $data[$age][$item['action']] = $item;
            $data[$age][GuildTreasury::ACTION_GPC_BUILDING]['amount'] *= -1;

        }
        $sum = array_sum(array_map(fn($v) => $v[GuildTreasury::ACTION_GPC_BUILDING]['amount'], $data));
        $sumPlayers = count($players);
        foreach ($data as $k => $item) {
            $data[$k]['costG'] = $item[GuildTreasury::ACTION_GPC_BUILDING]['amount']/$sum*100;
            $data[$k]['costP'] = count($eras[$k]??[])/$sumPlayers*100;
        }
        return $data;
    }


    public function groupedByPlayer(?array $criteria = [])
    {
        $qb = $this->createQueryBuilder('e');
        $qb
            ->select('SUM(e.amount) as amount', 'er.number as era', 'p.id', 'p.name', 'p.color', 'e.action')
            ->leftJoin('e.resource', 'r')
            ->leftJoin('r.era', 'er')
            ->leftJoin('e.player', 'p')
            ->where($qb->expr()->in('e.action', ['Wpłata do skarbca gildii','Produkcja budynku']))
            ->andWhere($qb->expr()->notIn('r.slug', ['guild_expedition_point','guild_championship_trophy_gold','guild_championship_trophy_silver','guild_championship_trophy_bronze']))
            ->groupBy('e.player')
            ->addGroupBy('e.action')
            ->addGroupBy('r.era')
            ->orderBy('p.name', 'ASC');

        $this->applyFilters($qb, $criteria);
        $data = [];

        $eras = range(0,20);
        foreach ($qb->getQuery()->getArrayResult() as $item) {
            $player = $item['name'];
            if(!isset($data[$player])) {
                $data[$player] = [
                    'id' => $item['id'],
                    'color' => $item['color'],
                    'p' => 0,
                    'medals' => 0,
                    'eras' => array_map(fn($v) => 0, range(0,20)),
                ];
            }
            if($item['action'] === 'Produkcja budynku') {
                $data[$player]['p'] += $item['amount'];
            }
            elseif($item['era'] === 1) {
                $data[$player]['medals'] += $item['amount'];
            }
            else {
                $data[$player]['eras'][$item['era']] += $item['amount'];
                unset($eras[$item['era']]);
            }
        }
        foreach ($data as $player => $datum) {

            foreach ($eras as $era) {
                unset($data[$player]['eras'][$era]);
            }
        }
        return $data;
    }
    public function oblegiGroupedByPlayer(?array $criteria = [])
    {
        $qb = $this->createQueryBuilder('e');
        $qb
            ->select('count(e.id) as amount', 'er.number as era', 'p.id', 'p.name', 'p.color')
            ->leftJoin('e.resource', 'r')
            ->leftJoin('r.era', 'er')
            ->leftJoin('e.player', 'p')
            ->where($qb->expr()->in('e.action', [GuildTreasury::ACTION_GVG_ARMY]))
            ->groupBy('e.player')
            ->addGroupBy('r.era')
            ->orderBy('p.name', 'ASC');

        $this->applyFilters($qb, $criteria);
        $data = [];
        foreach ($qb->getQuery()->getArrayResult() as $item) {
            $player = $item['name'];
            if(!isset($data[$player])) {
                $data[$player] = [
                    'id' => $item['id'],
                    'color' => $item['color'],
                    'eq13' => 0,
                    'gt13' => 0,
                    'eras' => array_combine(range(2,12), array_map(fn($v) => 0, range(2,12))),
                ];
            }
            if($item['era'] === 1) {
                $data[$player]['gt13'] += $item['amount'];
            }
            elseif($item['era'] < 13) {
                $data[$player]['eras'][$item['era']] += $item['amount'];
            }
            elseif($item['era'] == 13) {
                $data[$player]['eq13'] += $item['amount']/5;
            }
        }
        return $data;
    }

    public function gpcBuildings(?array $criteria = [])
    {
        $qb = $this->createQueryBuilder('e');
        $qb
            ->where($qb->expr()->in('e.action', [GuildTreasury::ACTION_GPC_BUILDING]))
            ->orderBy('e.id', 'ASC')
            ;

        $this->applyFilters($qb, $criteria);
        return $qb->getQuery()->getResult();
    }

    public function last(): \DateTime
    {
        $last =  $this->createQueryBuilder('e')
            ->orderBy('e.createdAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
        if($last) {
            return $last->getCreatedAt();
        }
        else {
            return (new \DateTime())->setTimestamp(0);
        }
    }
}
