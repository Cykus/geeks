<?php

namespace App\Repository;

use App\Entity\RawData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RawData|null find($id, $lockMode = null, $lockVersion = null)
 * @method RawData|null findOneBy(array $criteria, array $orderBy = null)
 * @method RawData[]    findAll()
 * @method RawData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RawDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RawData::class);
    }

    /**
     * @return RawData[] Returns an array of RawData objects
     */
    public function byWeek($type, $criteria = [])
    {
        $qb = $this->createQueryBuilder('r')
            ->select('r', "date_format(r.createdAt,'%Y-%u') as HIDDEN group_date")
            ->orderBy('r.createdAt', 'ASC')
            ->groupBy('group_date');
        $qb->where($qb->expr()->eq('r.type', ':type'))
            ->setParameter('type',$type)
        ;
        if(isset($criteria['dateFrom'])) {
            $from = clone $criteria['dateFrom'];
            $qb->andWhere($qb->expr()->gte('r.createdAt', ':dateFrom'))
                ->setParameter('dateFrom',$from)
            ;
        }
        if(isset($criteria['dateTo'])) {
            $to = clone $criteria['dateTo'];
            $to->add(new \DateInterval('P1D'));
            $qb->andWhere($qb->expr()->lt('r.createdAt', ':dateTo'))
                ->setParameter('dateTo',$to)
            ;
        }

        return $qb
            ->getQuery()
            ->getResult()
            ;
    }
    /**
     * @return RawData[] Returns an array of RawData objects
     */
    public function getByCriteria($type, $criteria = [])
    {
        $qb = $this->createQueryBuilder('r')
            ->orderBy('r.createdAt', 'ASC');
        $qb->where($qb->expr()->eq('r.type', ':type'))
            ->setParameter('type',$type)
        ;
        if(isset($criteria['dateFrom'])) {
            $from = clone $criteria['dateFrom'];
            $qb->andWhere($qb->expr()->gte('r.createdAt', ':dateFrom'))
                ->setParameter('dateFrom',$from)
            ;
        }
        if(isset($criteria['dateTo'])) {
            $to = clone $criteria['dateTo'];
            $to->add(new \DateInterval('P1D'));
            $qb->andWhere($qb->expr()->lt('r.createdAt', ':dateTo'))
                ->setParameter('dateTo',$to)
            ;
        }

        return $qb
            ->getQuery()
            ->getResult()
            ;
    }
    public function lastUpdate()
    {
        try {

            return new \DateTime($this->createQueryBuilder('e')
                ->select('e.createdAt')
                ->where('e.imported = :imported')
                ->setParameter('imported', true)
                ->orderBy('e.createdAt', 'DESC')
                ->setMaxResults(1)
                ->getQuery()
                ->getSingleScalarResult())
                ;
        } catch (\Exception) {
            return new \DateTime();
        }
    }

    /*
    public function findOneBySomeField($value): ?RawData
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
