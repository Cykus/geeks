<?php

namespace App\Repository;

use App\Entity\OtherGuildStats;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OtherGuildStats|null find($id, $lockMode = null, $lockVersion = null)
 * @method OtherGuildStats|null findOneBy(array $criteria, array $orderBy = null)
 * @method OtherGuildStats[]    findAll()
 * @method OtherGuildStats[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OtherGuildStatsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OtherGuildStats::class);
    }

    // /**
    //  * @return OtherGuildStats[] Returns an array of OtherGuildStats objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OtherGuildStats
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
