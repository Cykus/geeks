<?php

namespace App\Repository;

use App\Entity\GuildStatistic;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GuildStatistic|null find($id, $lockMode = null, $lockVersion = null)
 * @method GuildStatistic|null findOneBy(array $criteria, array $orderBy = null)
 * @method GuildStatistic[]    findAll()
 * @method GuildStatistic[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GuildStatisticRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GuildStatistic::class);
    }

//    /**
//     * @return GuildStatistic[] Returns an array of GuildStatistic objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GuildStatistic
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
