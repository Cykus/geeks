<?php

namespace App\Repository;

use App\Entity\Player;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Player|null find($id, $lockMode = null, $lockVersion = null)
 * @method Player|null findOneBy(array $criteria, array $orderBy = null)
 * @method Player[]    findAll()
 * @method Player[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlayerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Player::class);
    }

//    /**
//     * @return Player[] Returns an array of Player objects
//     */
    public function findByConflicts($conflicts)
    {
        $qb = $this->createQueryBuilder('p')
            ->leftJoin('p.adventuresStatistics', 'a')
            ->andWhere('p.active = :active')
            ->setParameter('active', true)
            ->orderBy('a.conflicts', 'DESC')
        ;
        if(is_numeric($conflicts)) {
            $qb
                ->andWhere($qb->expr()->lte('a.conflicts',':conflicts'))
                ->setParameter('conflicts', $conflicts)
            ;
        }
        return $qb->getQuery()->getResult();
    }
//    /**
//     * @return Player[] Returns an array of Player objects
//     */
    public function findByBattlegroundsPoints($points)
    {
        $qb = $this->createQueryBuilder('p')
            ->leftJoin('p.battlegroundsStatistics', 'a')
            ->andWhere('p.active = :active')
            ->setParameter('active', true)
            ->orderBy('a.score', 'DESC')
        ;
        if(is_numeric($points)) {
            $qb
                ->andWhere($qb->expr()->lte('a.score',':score'))
                ->setParameter('score', $points)
            ;
        }
        return $qb->getQuery()->getResult();
    }
    public function getLastBattleStatsDiff($date)
    {
        dd($date);
    }

    /*
    public function findOneBySomeField($value): ?Player
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
