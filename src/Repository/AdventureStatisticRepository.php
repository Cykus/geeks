<?php

namespace App\Repository;

use App\Entity\AdventureStatistic;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AdventureStatistic|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdventureStatistic|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdventureStatistic[]    findAll()
 * @method AdventureStatistic[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdventureStatisticRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdventureStatistic::class);
    }

//    /**
//     * @return AdventureStatistic[] Returns an array of AdventureStatistic objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AdventureStatistic
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
