<?php

namespace App\Repository;

use App\Entity\BattlegroundProvince;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BattlegroundProvince|null find($id, $lockMode = null, $lockVersion = null)
 * @method BattlegroundProvince|null findOneBy(array $criteria, array $orderBy = null)
 * @method BattlegroundProvince[]    findAll()
 * @method BattlegroundProvince[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BattlegroundProvinceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BattlegroundProvince::class);
    }

    // /**
    //  * @return BattlegroundProvince[] Returns an array of BattlegroundProvince objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BattlegroundProvince
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
