<?php

namespace App\Repository;

use App\Entity\GuildResource;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use function Doctrine\ORM\QueryBuilder;

/**
 * @method GuildResource|null find($id, $lockMode = null, $lockVersion = null)
 * @method GuildResource|null findOneBy(array $criteria, array $orderBy = null)
 * @method GuildResource[]    findAll()
 * @method GuildResource[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GuildResourceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GuildResource::class);
    }

//    /**
//     * @return GuildResource[] Returns an array of GuildResource objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GuildResource
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    /**
     * @return GuildResource[] Returns an array of Resource objects
     */
    public function getIndexedBySlug()
    {
        $qb = $this->createQueryBuilder('e');
        $tmp = $qb
            ->leftJoin('e.resource', 'r')
            ->where($qb->expr()->eq('IDENTITY(e.rawData)', '('.
                $this->createQueryBuilder('o')
                    ->select('MAX(IDENTITY(o.rawData))')
                    ->getDQL()
                .')'))
            ->getQuery()
            ->getResult();
        $data = [];
        foreach ($tmp as $item) {
            $data[$item->getResource()->getSlug()] = $item;
        }
        return $data;
    }
}
