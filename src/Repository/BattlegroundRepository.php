<?php

namespace App\Repository;

use App\Entity\Battleground;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use function Doctrine\ORM\QueryBuilder;

/**
 * @method Battleground|null find($id, $lockMode = null, $lockVersion = null)
 * @method Battleground|null findOneBy(array $criteria, array $orderBy = null)
 * @method Battleground[]    findAll()
 * @method Battleground[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BattlegroundRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Battleground::class);
    }

    // /**
    //  * @return Battleground[] Returns an array of Battleground objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findOneByDate(\DateTimeInterface $date): ?Battleground
    {
        $qb = $this->createQueryBuilder('e');
        return $qb->andWhere($qb->expr()->gte('e.endsAt',':date'))
            ->setParameter('date', $date)
            ->orderBy('e.endsAt', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
