<?php

namespace App\Repository;

use App\Entity\BattlegroundProvinceData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BattlegroundProvinceData|null find($id, $lockMode = null, $lockVersion = null)
 * @method BattlegroundProvinceData|null findOneBy(array $criteria, array $orderBy = null)
 * @method BattlegroundProvinceData[]    findAll()
 * @method BattlegroundProvinceData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BattlegroundProvinceDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BattlegroundProvinceData::class);
    }

    // /**
    //  * @return BattlegroundProvinceData[] Returns an array of BattlegroundProvinceData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BattlegroundProvinceData
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
