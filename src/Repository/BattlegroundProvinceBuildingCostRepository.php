<?php

namespace App\Repository;

use App\Entity\Battleground;
use App\Entity\BattlegroundBuilding;
use App\Entity\BattlegroundProvinceBuildingCost;
use App\Entity\Era;
use App\Entity\GuildTreasury;
use App\Entity\Player;
use App\Entity\Resource;
use App\Handler\RawDataHandler;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BattlegroundProvinceBuildingCost|null find($id, $lockMode = null, $lockVersion = null)
 * @method BattlegroundProvinceBuildingCost|null findOneBy(array $criteria, array $orderBy = null)
 * @method BattlegroundProvinceBuildingCost[]    findAll()
 * @method BattlegroundProvinceBuildingCost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BattlegroundProvinceBuildingCostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BattlegroundProvinceBuildingCost::class);
    }

    public function applyFilters(QueryBuilder $qb, array $criteria): void {
        $qb->leftJoin('e.building', 'bb')
            ->leftJoin('bb.province', 'pp')
            ->andWhere($qb->expr()->gt('pp.slots', ':slots'))
            ->setParameter('slots', 0);
        if(isset($criteria['era']) && $criteria['era'] instanceof Era) {
            $qb->leftJoin('e.resource', 'rrr')
                ->andWhere($qb->expr()->eq('rrr.era', ':era'))
                ->setParameter('era', $criteria['era']);
        }
        if(isset($criteria['building']) && $criteria['building'] instanceof BattlegroundBuilding) {
            $qb->andWhere($qb->expr()->eq('bb.building', ':building'))
                ->setParameter('building', $criteria['building']);
        }
        if(isset($criteria['player']) && $criteria['battleground'] instanceof Battleground) {
            $qb->andWhere($qb->expr()->eq('pp.battleground', ':battleground'))
                ->setParameter('battleground', $criteria['battleground']);
        }
    }

    public function list(?array $criteria = [])
    {
        $qb = $this->createQueryBuilder('e');
        $this->applyFilters($qb, $criteria);
        $qb->select('SUM(e.amount*pp.slots) as amount', 'er.number as era', 'er.color','r.slug','r.name')
            ->leftJoin('e.resource', 'r')
            ->leftJoin('r.era', 'er')
            ->groupBy('r.id')
            ->orderBy('er.number', 'ASC');
        $data = array_map(function ($v) {
            $v['color'] = RawDataHandler::hex2rgba($v['color'] ,0.2);
            return $v;
        }, $qb->getQuery()->getResult());

        $eras = [];
        foreach($data as $datum)
        {
            if(!isset($eras[$datum['era']])) {
                $eras[$datum['era']] = [
                    'resources' => [],
                    'sum' => 0,
                ];
            }

            $eras[$datum['era']]['resources'][] = $datum;
            $eras[$datum['era']]['sum'] += $datum['amount'];
        }
        krsort($eras);
        return $eras;
    }
    // /**
    //  * @return BattlegroundProvinceBuildingCost[] Returns an array of BattlegroundProvinceBuildingCost objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BattlegroundProvinceBuildingCost
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
