<?php

namespace App\Repository;

use App\Entity\Battleground;
use App\Entity\BattlegroundProvinceBuilding;
use App\Entity\GuildTreasury;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;
use function Doctrine\ORM\QueryBuilder;

/**
 * @method BattlegroundProvinceBuilding|null find($id, $lockMode = null, $lockVersion = null)
 * @method BattlegroundProvinceBuilding|null findOneBy(array $criteria, array $orderBy = null)
 * @method BattlegroundProvinceBuilding[]    findAll()
 * @method BattlegroundProvinceBuilding[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BattlegroundProvinceBuildingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BattlegroundProvinceBuilding::class);
    }

    public function list(Battleground $battleground, ?array $criteria = [])
    {
        /** @var GuildTreasury[] $resources */
        $guildTreasures = $this->_em->getRepository(GuildTreasury::class)->gpcBuildings($criteria);
        $data = [];
        foreach ($guildTreasures as $guildTreasury) {
            if(!isset($data[$guildTreasury->getCreatedAt()->getTimestamp()])) {
                $data[$guildTreasury->getCreatedAt()->getTimestamp()] = [];

            }
            $data[$guildTreasury->getCreatedAt()->getTimestamp()][] = $guildTreasury;

        }
        /** @var GuildTreasury[] $datum */
        foreach ($data as $time => $datum) {
            $buildings = $this->getBuildingFromTreasury($battleground, $datum);
        }

        return $data;
    }
    private function getBuildingFromTreasury(Battleground $battleground, array $d) {
        $buildings = [];
        $oldD = $d;

        $cc = count($d);
        for ($i = 0; $i< $cc;$i++) {
            $costsCount = 0;
            $building = null;
            foreach ($d as $k => $dd) {
                $qb = $this->createQueryBuilder('e')->setMaxResults(1);
                $costsCount += 1;
                $qb->join('e.costs', 'c'.$k)
                    ->andWhere($qb->expr()->andX($qb->expr()->eq('c'.$k.'.resource', $dd->getResource()->getId()), $qb->expr()->eq('c'.$k.'.amount', abs($dd->getAmount()))));
                try {

                    /** @var BattlegroundProvinceBuilding $building */
                    $building = $qb->getQuery()->getSingleResult();
                }
                catch (NoResultException) {
                    $building = null;
                    break 2;
                }

            }
            if ($building) {
                if($costsCount < $building->getCosts()->count()) {
                    break;
                }
                $buildings[] = $building;
                $buildings = array_merge($buildings, $this->getBuildingFromTreasury($battleground, array_slice($oldD, $costsCount)));
            }
            array_pop($d);
        }
        return $buildings;

    }
    // /**
    //  * @return BattlegroundProvinceBuilding[] Returns an array of BattlegroundProvinceBuilding objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BattlegroundProvinceBuilding
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
