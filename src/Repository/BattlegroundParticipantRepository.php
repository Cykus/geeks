<?php

namespace App\Repository;

use App\Entity\BattlegroundParticipant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BattlegroundParticipant|null find($id, $lockMode = null, $lockVersion = null)
 * @method BattlegroundParticipant|null findOneBy(array $criteria, array $orderBy = null)
 * @method BattlegroundParticipant[]    findAll()
 * @method BattlegroundParticipant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BattlegroundParticipantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BattlegroundParticipant::class);
    }

    // /**
    //  * @return BattlegroundParticipant[] Returns an array of BattlegroundParticipant objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BattlegroundParticipant
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
