<?php

namespace App\Form;

use App\Entity\BattlegroundBuilding;
use App\Entity\Era;
use App\Entity\GuildTreasury;
use App\Entity\Player;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\Contracts\Translation\TranslatorInterface;

class SearchBuildingsCostType extends AbstractType
{
    public function __construct(private readonly TranslatorInterface $translator)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('era', EntityType::class, [
                'required' => false,
                'class' => Era::class,
                'choice_label' => fn($era) => $this->translator->trans('era.i'.$era->getNumber()),
                'query_builder' => fn(EntityRepository $er) => $er->createQueryBuilder('u')
                    ->orderBy('u.number', 'ASC'),
            ])
            ->add('building', EntityType::class, [
                'required' => false,
                'class' => BattlegroundBuilding::class,
                'choice_name' => 'name',
            ])
        ;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'method' => 'GET',
            'allow_extra_fields' => true,
            'csrf_protection' => false,

        ]);
    }

    public function getBlockPrefix(): string {
        return '';
    }
}