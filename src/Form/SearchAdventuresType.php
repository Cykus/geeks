<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchAdventuresType extends SearchStatisticsType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('conflicts', IntegerType::class, [
                'label' => 'Liczba konfliktów'
            ])
            ->remove('sort')
        ;
        $builder->remove('current');
    }

    public function getBlockPrefix(): string {
        return '';
    }
}
