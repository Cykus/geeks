<?php

namespace App\Form;

use App\Entity\Era;
use App\Entity\GuildTreasury;
use App\Entity\Player;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\Contracts\Translation\TranslatorInterface;

class SearchGuildTreasuryType extends AbstractType
{
    public function __construct(private readonly TranslatorInterface $translator)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('player', EntityType::class, [
                'required' => false,
                'class' => Player::class,
                'query_builder' => fn(EntityRepository $er) => $er->createQueryBuilder('u')
                    ->orderBy('u.name', 'ASC'),
            ])
            ->add('action', ChoiceType::class, [
                'required' => false,
                'choices' => GuildTreasury::getActionChoices()
            ])
            ->add('era', EntityType::class, [
                'required' => false,
                'class' => Era::class,
                'choice_label' => fn($era) => $this->translator->trans('era.i'.$era->getNumber()),
                'query_builder' => fn(EntityRepository $er) => $er->createQueryBuilder('u')
                    ->orderBy('u.number', 'ASC'),
            ])
        ;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'method' => 'GET',
            'allow_extra_fields' => true,
            'csrf_protection' => false,

        ]);
    }

    public function getBlockPrefix(): string {
        return '';
    }
}