<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchBattlegroundsType extends SearchStatisticsType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('points', IntegerType::class, [
                'label' => 'Liczba punktów'
            ])
            ->remove('sort')
        ;
        $builder->remove('current');
    }

    public function getBlockPrefix(): string {
        return '';
    }
}
