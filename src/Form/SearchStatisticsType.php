<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchStatisticsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('current', CheckboxType::class, [
                'required' => false
            ])
            ->add('dateFrom', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Od'
            ])
            ->add('dateTo', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Do'
            ])
            ->add('sort', ChoiceType::class, [
                'choices'  => [
                    'punkty' => 'pointsSum',
                    'przyrost' => 'points',
                ],
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'method' => 'GET',
            'allow_extra_fields' => true,
            'csrf_protection' => false,

        ]);
    }

    public function getBlockPrefix(): string {
        return '';
    }
}
