<?php
/**
 * Created by PhpStorm.
 * User: cykus
 * Date: 18.06.18
 * Time: 18:33
 */

namespace App\Handler;


use App\Entity\AdventureStatistic;
use App\Entity\Battleground;
use App\Entity\BattlegroundStatistic;
use App\Entity\Construction;
use App\Entity\ConstructionStatistic;
use App\Entity\Era;
use App\Entity\GuildResource;
use App\Entity\GuildTreasury;
use App\Entity\OtherGuildStats;
use App\Entity\Player;
use App\Entity\RawData;
use App\Entity\Resource;
use App\Entity\Statistic;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use function Doctrine\ORM\QueryBuilder;

class StatisticHandler
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }


    public function preparePlayerData($statistics, $data, $battles = false): array {

        if($data['current']) {
            $players = $this->entityManager->getRepository(Player::class)->findBy(['active'=>true],['rank'=>'ASC']);
        }
        else {
            $players = $this->entityManager->getRepository(Player::class)->findBy([],['rank'=>'ASC']);
        }
        $dates = [];
        $series = [];
        foreach ($players as $player) {
            $series[$player->getId()] = [
                'id' => $player->getId(),
                'label' => (string)$player,
                'backgroundColor' => $player->getColor(),
                'borderColor' => $player->getColor(),
                'fill' => false,
                'data' => [],
                'additionalData' => $player->getData(),
            ];
        }
        /** @var RawData $statistic */
        foreach ($statistics as $statistic) {
            $dates[] = $statistic->getCreatedAt()->getTimestamp();
            foreach ($series as $k => $s) {
                if(isset($series[$k])) {
                    $series[$k]['data'][$statistic->getCreatedAt()->getTimestamp()] = null;
                }
            }
            foreach ($statistic->getStatistics() as $stat) {
                if(isset($series[$stat->getPlayer()->getId()])) {
                    $series[$stat->getPlayer()->getId()]['data'][$stat->getRawData()->getCreatedAt()->getTimestamp()] = $battles?$stat->getBattles():$stat->getScore();
                }
            }
        }
        $series = array_values($series);
        foreach ($series as $k => $v) {
            $series[$k]['data'] = array_values($v['data']);

        }

        $data = [
            'labels' => $dates,
            'datasets' => $series,
        ];
        return $data;
    }
    public function preparePlayerDetailData($statistics, $data, Player $player): array {

        $dates = [];
        $series = [];
        foreach (['score', 'battles', 'eraName'] as $field) {
            $series[$field] = [
                'label' => $field,
                'fill' => false,
                'data' => [],
            ];
        }
        /** @var RawData $statistic */
        foreach ($statistics as $statistic) {
            $dates[] = $statistic->getCreatedAt()->getTimestamp();
            foreach ($series as $k => $s) {
                if(isset($series[$k])) {
                    $series[$k]['data'][$statistic->getCreatedAt()->getTimestamp()] = null;
                }
            }
            foreach ($statistic->getStatistics() as $stat) {
                if($stat->getPlayer()->getId() === $player->getId()) {
                    $series['score']['data'][$stat->getRawData()->getCreatedAt()->getTimestamp()] = $stat->getScore();
                    $series['battles']['data'][$stat->getRawData()->getCreatedAt()->getTimestamp()] = $stat->getBattles();
                    $series['eraName']['data'][$stat->getRawData()->getCreatedAt()->getTimestamp()] = $stat->getEra();
                }
            }
        }
        $series = array_values($series);
        foreach ($series as $k => $v) {
            $series[$k]['data'] = array_values($v['data']);

        }

        $data = [
            'labels' => $dates,
            'datasets' => $series,
        ];
        return $data;
    }

    public function prepareAdventuresPlayerData($statistics, $criteria): array {

        /** @var Player[] $players */
        $players = $this->entityManager->getRepository(Player::class)->findByConflicts($criteria['conflicts']);
        $dates = [];
        $series = [];
        foreach ($players as $player) {
            $series[$player->getId()] = [
                'id' => $player->getId(),
                'label' => (string)$player,
                'backgroundColor' => $player->getColor(),
                'borderColor' => $player->getColor(),
                'fill' => false,
                'data' => [],
                'additionalData' => $player->getData(),
              ];
        }
        /** @var RawData $statistic */
        foreach ($statistics as $statistic) {
            $dates[] = $statistic->getCreatedAt()->getTimestamp();
            foreach ($series as $k => $s) {
                if(isset($series[$k])) {
                    $series[$k]['data'][$statistic->getCreatedAt()->getTimestamp()] = null;
                }
            }

            /** @var AdventureStatistic $stat */
            foreach ($statistic->getAdventuresStatistics() as $stat) {
                if(isset($series[$stat->getPlayer()->getId()])) {
                    $series[$stat->getPlayer()->getId()]['data'][$stat->getRawData()->getCreatedAt()->getTimestamp()] = $stat->getConflicts();
                }
            }
        }

        $series = array_values($series);

        $avgs = [];
        foreach ($series as $k => $v) {
            $series[$k]['data'] = array_values($v['data']);
            $tmp = array_filter( $series[$k]['data'], 'strlen' );
            if(count($tmp)) {
                $avgs[$k] = array_sum($tmp) / count($tmp);
            }
            else {
                $avgs[$k] = 0;
            }
        }
        array_multisort($avgs, SORT_DESC, $series);

        $sum = [
            'label' => 'Średnia',
            'warning' => false,
            'backgroundColor' => '#FFF',
            'borderColor' => '#FFF',
            'fill' => false,
            'data' => [],
            'additionalData' => [],
        ];
        foreach ($series[0]['data'] as $k => $v) {
            $s = [];
            foreach ($series as $ks => $vs) {
                $s[] = $vs['data'][$k];
            }
            $s = array_filter( $s, 'strlen' );
            if(count($s)) {
                $sum['data'][$k] = array_sum($s)/count($s);
            }
            else {
                $sum['data'][$k]  = 0;
            }
        }
        $series[] = $sum;
        $data = [
            'labels' => $dates,
            'datasets' => $series,
        ];
        return $data;
    }
    public function prepareBattlegroundsData($statistics, $criteria): array {

        /** @var Player[] $players */
        $players = $this->entityManager->getRepository(Player::class)->findByBattlegroundsPoints($criteria['points']);
        $dates = [];
        $ids = [];
        $series = [];
        foreach ($players as $player) {
            $series[$player->getId()] = [
                'id' => $player->getId(),
                'label' => (string)$player,
                'backgroundColor' => $player->getColor(),
                'borderColor' => $player->getColor(),
                'fill' => false,
                'data' => [],
                'additionalData' => $player->getData(),
              ];
        }
        /** @var RawData $statistic */
        foreach ($statistics as $statistic) {
            $dates[] = $statistic->getCreatedAt()->getTimestamp();
            $ids[] = $statistic->getId();
            foreach ($series as $k => $s) {
                if(isset($series[$k])) {
                    $series[$k]['data'][$statistic->getCreatedAt()->getTimestamp()] = [
                        'score' => null,
                        'negotiations' => null,
                        'battles' => null,
                    ];
                }
            }

            /** @var BattlegroundStatistic $stat */
            foreach ($statistic->getBattlegroundsStatistics() as $stat) {
                if(isset($series[$stat->getPlayer()->getId()])) {
                    $series[$stat->getPlayer()->getId()]['data'][$stat->getRawData()->getCreatedAt()->getTimestamp()] = [
                        'score' => $stat->getScore(),
                        'negotiations' => $stat->getNegotiations(),
                        'battles' => $stat->getBattles(),
                    ];
                }
            }
        }
        $series = array_values($series);
        foreach ($series as $k => $v) {
            $series[$k]['data'] = array_values($v['data']);
        }
        $data = [
            'labels' => $dates,
            'ids' => $ids,
            'datasets' => $series,
        ];
        return $data;
    }

    public function prepareGuildData($statistics): array {
        return $statistics;
    }
    public function getChartOptions(): array {
        $timeFormat = 'DD-MM-YYYY';
        $data = [
            'title' => null,
            'scales' => [
                'y' => [
                    'ticks' => [
                        'fontColor' => '#FFF'
                    ]
                ],
                'x' => [
                    'ticks' => [
                        'fontColor' => '#FFF',
                        'source' => 'labels'
                    ],
                    'type' => 'time',
                    'time' => [
                        'displayFormats' => [
                            'millisecond' => $timeFormat,
                            'second' => $timeFormat,
                            'minute' => $timeFormat,
                            'hour' => $timeFormat,
                            'day' => $timeFormat,
                            'week' => $timeFormat,
                            'month' => $timeFormat,
                            'quarter' => $timeFormat,
                            'year' => $timeFormat,
                        ],
                        'tooltipFormat' => $timeFormat,
                    ]
                ] ,
                'legend' => [
                    'labels' => [
                        'fontColor' => '#FFF'
                    ],
                ],

            ]
        ];

        return $data;
    }
    public function parseChartData($data): array {
        foreach ($data['labels'] as $k => $datum) {
            $data['labels'][$k] = $datum*1000;
        }
        foreach ($data['datasets'] as $k => $v) {
            foreach ($v['data'] as $kk => $vv) {
                $data['datasets'][$k]['data'][$kk]  = ['x' => $kk, 'y' => is_array($vv)?$vv['score']:$vv];
            }
        }

        return $data;
    }
    public function prepareGuildResourceData($statistics): array {
        $qb = $this->entityManager->getRepository(Resource::class)->createQueryBuilder('e');
        $qb
            ->leftJoin('e.era', 'er')
            ->where($qb->expr()->gt('er.number', 1))
            ->orderBy('e.era', 'ASC');
        /** @var Resource[] $resources */
        $resources = $qb->getQuery()->getResult();
        $dates = [];
        $series = [];
        foreach ($resources as $resource) {
            $series[$resource->getId()] = [
                'era' => $resource->getEra(),
                'bgColor' => $resource->getEra()->getRGBAColor(),
                'backgroundColor' => $resource->getEra()->getRGBAColor(),
                'borderColor' => $resource->getEra()->getRGBAColor(),
                'slug' => $resource->getSlug(),
                'label' => $resource->getName(),
                'fill' => false,
                'data' => [],
            ];
        }
        /** @var RawData $statistic */
        foreach ($statistics as $statistic) {
            $dates[] = $statistic->getCreatedAt()->getTimestamp();
            foreach ($series as $k => $s) {
                if(isset($series[$k])) {
                    $series[$k]['data'][$statistic->getCreatedAt()->getTimestamp()] = null;
                }
            }

            /** @var AdventureStatistic $stat */
            foreach ($statistic->getGuildResources() as $stat) {
                if(isset($series[$stat->getResource()->getId()])) {
                    $series[$stat->getResource()->getId()]['data'][$stat->getRawData()->getCreatedAt()->getTimestamp()] = $stat->getScore();
                }
            }
        }

        $series = array_values($series);

        foreach ($series as $k => $v) {
            $series[$k]['data'] = array_values($v['data']);
        }

        $data = [
            'labels' => $dates,
            'datasets' => $series,
        ];
        return $data;
    }
    public function prepareUserResourceData($statistics): array {
        $qb = $this->entityManager->getRepository(Resource::class)->createQueryBuilder('e');
        $qb
            ->leftJoin('e.era', 'er')
            ->where($qb->expr()->gt('er.number', 1))
            ->orderBy('e.era', 'ASC');
        /** @var Resource[] $resources */
        $resources = $qb->getQuery()->getResult();
        $dates = [];
        $series = [];
        foreach ($resources as $resource) {
            $series[$resource->getId()] = [
                'era' => $resource->getEra(),
                'bgColor' => $resource->getEra()->getRGBAColor(),
                'backgroundColor' => $resource->getEra()->getRGBAColor(),
                'borderColor' => $resource->getEra()->getRGBAColor(),
                'slug' => $resource->getSlug(),
                'label' => $resource->getName(),
                'fill' => false,
                'data' => [],
            ];
        }
        /** @var RawData $statistic */
        foreach ($statistics as $statistic) {
            $dates[] = $statistic->getCreatedAt()->getTimestamp();
            foreach ($series as $k => $s) {
                if(isset($series[$k])) {
                    $series[$k]['data'][$statistic->getCreatedAt()->getTimestamp()] = null;
                }
            }

            /** @var AdventureStatistic $stat */
            foreach ($statistic->getGuildResources() as $stat) {
                if(isset($series[$stat->getResource()->getId()])) {
                    $series[$stat->getResource()->getId()]['data'][$stat->getRawData()->getCreatedAt()->getTimestamp()] = $stat->getScore();
                }
            }
        }

        $series = array_values($series);

        foreach ($series as $k => $v) {
            $series[$k]['data'] = array_values($v['data']);
        }

        $data = [
            'labels' => $dates,
            'datasets' => $series,
        ];
        return $data;
    }

    public function prepareBattlegroundData(Battleground  $battleground): Battleground
    {
        /** @var GuildResource[] $guildResources */
        $guildResourcesByEras = $this->entityManager->getRepository(GuildTreasury::class)->groupedByAge([
            'dateFrom' => $battleground->getStartsAt(),
            'dateTo' => $battleground->getEndsAt(),
        ]);
        $resourcesData = [];
        foreach ($guildResourcesByEras as $guildResourcesByEra) {
            $resourcesData = $resourcesData + $guildResourcesByEra['resources']['sum'];
        }
        foreach ($battleground->getProvinces() as $province) {
            $provinceComputedData = ['lowResources' => null];
            foreach ($province->getBuildings() as $building) {
                $buildingComputedData = ['lowResources' => null];
                foreach ($building->getCosts() as $cost) {
                    $amount = (($this->entityManager->getRepository(Era::class)->getLastNumber()-$cost->getResource()->getEra()->getNumber()+1)/10)*$resourcesData[$cost->getResource()->getId()]['amount'];
                    $resources = ['lowResources' => null, 'resources' => $amount];
                    if($amount < -25000) {
                        $color = '#d99797';
                        if($amount < -50000) {
                            $color = '#D28282';
                        }
                        elseif($amount < -75000) {
                            $color = '#D55151';
                        }
                        elseif($amount < -100000) {
                            $color = '#E34848';
                        }
                        $resources['lowResources'] = $color;
                        $buildingComputedData['lowResources'] = $color;
                        if(in_array($building->getBuilding()->getSlug(), ['siege_camp', 'traps'])) {
                            $provinceComputedData['lowResources'] = $color;
                        }
                    }
                    $cost->setComputedData($resources);
                }
                $building->setComputedData($buildingComputedData);
            }
            $province->setComputedData($provinceComputedData);
        }
        return $battleground;
    }
    public function prepareOtherGuildStats(OtherGuildStats $otherGuildStats): array
    {
        $stats = $otherGuildStats->getStats();
        $data = [
            'name' => $stats['currentData']['name'],
            'from' => (new \DateTime())->setTimestamp((int)$stats['prevData']['timestamp']),
            'to' => (new \DateTime())->setTimestamp((int)$stats['currentData']['timestamp']),

        ];
        $data['diff'] = $data['from']->diff($data['to'])->format(' %i minut, %s sekund');
        $diffData = [];
        foreach ($stats['prevData']['members'] as $item) {
            $diffData[$item['player_id']] = [
                'name' => $item['name'],
                'prev' => $item['won_battles'],
                'current' => 0,
                'diff' => 0,
                'perSec' => 0,
            ];
        }
        $timeDiff = $data['to']->getTimestamp() - $data['from']->getTimestamp();;
        foreach ($stats['currentData']['members'] as $item) {
            $diffData[$item['player_id']]['current'] = $item['won_battles'];
            $diffData[$item['player_id']]['diff'] = $item['won_battles'] - $diffData[$item['player_id']]['prev'];
            $diffData[$item['player_id']]['perSec'] = round($diffData[$item['player_id']]['diff']/$timeDiff*100)/100;
        }
        uasort($diffData, fn($a, $b) => $b['diff'] <=> $a['diff']);

        $diffData = array_filter($diffData, fn($v) => $v['diff'] > 0);
        $data['diffData'] = $diffData;

        return $data;
    }
}