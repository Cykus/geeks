<?php


namespace App\Handler;


use App\Entity\AdventureStatistic;
use App\Entity\Battleground;
use App\Entity\BattlegroundBuilding;
use App\Entity\BattlegroundParticipant;
use App\Entity\BattlegroundProvince;
use App\Entity\BattlegroundProvinceBuilding;
use App\Entity\BattlegroundProvinceBuildingCost;
use App\Entity\BattlegroundProvinceData;
use App\Entity\BattlegroundStatistic;
use App\Entity\Era;
use App\Entity\GuildResource;
use App\Entity\GuildStatistic;
use App\Entity\GuildTreasury;
use App\Entity\Player;
use App\Entity\RawData;
use App\Entity\Resource;
use App\Entity\Statistic;
use Deviantintegral\Har\Har;
use Deviantintegral\Har\Header;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class RawDataHandler
{
    public function __construct(private readonly EntityManagerInterface $entityManager, private readonly LoggerInterface $logger)
    {
    }

    public function importHAR(Har $har, $filename): array {
        $rawData = [
            RawData::TYPE_STATISTICS => null,
            RawData::TYPE_RESOURCES => null,
            RawData::TYPE_BATTLEGROUNDS => null,
            RawData::TYPE_ADVENTURES => null,
            RawData::TYPE_RESOURCES_LOGS => null,
            RawData::TYPE_BATTLEGROUND => null,
            RawData::TYPE_BATTLEGROUND_PROVINCE => null,
        ];

        $index = 0;
        foreach ($har->getLog()->getEntries() as $entry) {
            $response = $entry->getResponse();
            if($this->getHeader($response->getHeaders(), 'content-type') === 'application/json') {
                $data = json_decode($response->getContent()->getText(), true);
                foreach ($data as $item) {
                    if(!is_array($item) || !isset($item['requestMethod'])) continue;
                    $rawDatum = new RawData();
                    if($item['requestMethod'] == 'getOwnClanData') {
                        $rawDatum->setType(RawData::TYPE_STATISTICS);
                    }
                    elseif($item['requestMethod'] == 'getTreasury') {
                        $rawDatum->setType(RawData::TYPE_RESOURCES);
                    }
                    elseif($item['requestMethod'] == 'getTreasuryLogs' && count($item['responseData']['logs'])> 1000) {
                        $rawDatum->setType(RawData::TYPE_RESOURCES_LOGS);
                        $index++;
                    }
                    elseif($item['requestMethod'] == 'getContributionList') {
                        $rawDatum->setType(RawData::TYPE_ADVENTURES);
                    }
                    elseif($item['requestClass'] == 'GuildBattlegroundStateService' && $item['requestMethod'] == 'getState') {
                        $rawDatum->setType(RawData::TYPE_BATTLEGROUNDS);
                    }
                    elseif($item['requestClass'] == 'GuildBattlegroundService' && $item['requestMethod'] == 'getBattleground') {
                        $rawDatum->setType(RawData::TYPE_BATTLEGROUND);
                    }
                    elseif($item['requestClass'] == 'GuildBattlegroundBuildingService' && $item['requestMethod'] == 'getBuildings') {
                        $rawDatum->setType(RawData::TYPE_BATTLEGROUND_PROVINCE);
                        $index++;
                    }
                    elseif($item['requestClass'] == 'OtherPlayerService' && $item['requestMethod'] == 'visitPlayer') {
                        $rawDatum->setType(RawData::TYPE_CITY);
                        $index++;
                    }
                    elseif($item['requestClass'] == 'StartupService' && $item['requestMethod'] == 'getData') {
                        $d = $item['responseData'];
                        $item['responseData'] = [
                            'city_map' => $d['city_map'],
                            'city_name' => $d['user_data']['city_name'],
                            'other_player' => $d['user_data'],
                            'other_player_era' => $d['user_data']['era'],
                        ];
                        foreach ($d['socialbar_list'] as $i) {
                            if($i['player_id']==$d['user_data']['player_id']) {
                                $item['responseData']['other_player'] = $i;
                                break;
                            }
                            
                        }
                        $rawDatum->setType(RawData::TYPE_CITY);
                        $index++;
                    }
                    if($rawDatum->getType()) {
                        $rawDatum->setCreatedAt($entry->getStartedDateTime());
                        $rawDatum->setData($item['responseData']);
                        $rawDatum->setFilename($filename);
                        if(in_array($rawDatum->getType(), [RawData::TYPE_CITY, RawData::TYPE_BATTLEGROUND_PROVINCE])) {
                            $rawData[$rawDatum->getType().'_'.$index] = $rawDatum;
                        }
                        else {
                            $rawData[$rawDatum->getType()] = $rawDatum;
                        }
                    }
                }
            }
            
        }
        if($rawData[RawData::TYPE_BATTLEGROUND]) {
            $rawData[RawData::TYPE_BATTLEGROUNDS] = null;
        }
        $rawData = array_filter($rawData);
        foreach ($rawData as $rawDatum) {
            $this->entityManager->persist($rawDatum);
            $this->entityManager->flush();
        }
        return $rawData;
    }

    public function import() {

        $rawData = $this->entityManager->getRepository(RawData::class)->findBy(['imported'=>false]);
        /** @var RawData $rawDatum */
        foreach ($rawData as $rawDatum) {
            $this->logger->info('Importing RawData', [
                'id' => $rawDatum->getId(),
                'type' => $rawDatum->getType(),
            ]);
            if($rawDatum->getType() == RawData::TYPE_STATISTICS) {
                $this->importOwnClanData($rawDatum, $rawDatum->getData());
            }
            elseif($rawDatum->getType() == RawData::TYPE_RESOURCES) {
                $this->importResourcesData($rawDatum, $rawDatum->getData());
            }
            elseif($rawDatum->getType() == RawData::TYPE_ADVENTURES) {
                $this->importAdventuresData($rawDatum, $rawDatum->getData());
            }
            elseif($rawDatum->getType() == RawData::TYPE_BATTLEGROUNDS) {
                $this->importBattlegroundsData($rawDatum, $rawDatum->getData()['playerLeaderboardEntries']);
            }
            elseif($rawDatum->getType() == RawData::TYPE_RESOURCES_LOGS) {
                $this->importResourcesLogsData($rawDatum, $rawDatum->getData()['logs']);
            }
            elseif($rawDatum->getType() == RawData::TYPE_BATTLEGROUND) {
                $this->importBattlegroundData($rawDatum, $rawDatum->getData());
            }
            elseif($rawDatum->getType() == RawData::TYPE_BATTLEGROUND_PROVINCE) {
                $this->importBattlegroundProvinceData($rawDatum, $rawDatum->getData());
            }
            $rawDatum->setImported(true); 
            $this->entityManager->persist($rawDatum);
        }
        $this->entityManager->flush();
    }
    public function importUpdate() {

        $rawData = $this->entityManager->getRepository(RawData::class)->findBy(['type'=> RawData::TYPE_STATISTICS]);
        /** @var RawData $rawDatum */
        foreach ($rawData as $rawDatum) {
            foreach ($rawDatum->getData()['members'] as $member) {
                foreach ($rawDatum->getStatistics() as $statistic) {
                    if($member['player_id'] == $statistic->getPlayer()->getPlayerId()) {
                        $era = $this->entityManager->getRepository(Era::class)->findOneBy(['slug' => $member['era']]);
                        $statistic->setEra($era);
                        $statistic->getPlayer()->setEra($era);
                        $this->entityManager->persist($statistic);
                        $this->entityManager->flush();
                    }
                }
            }
        }
        $this->entityManager->flush();
    }
    public function importOwnClanData(RawData $rawData, $data) {
        $qb = $this->entityManager->getRepository(Player::class)->createQueryBuilder('e');
        $qb->update()
            ->set('e.active', ':active')
            ->setParameter('active', false);
        $qb->getQuery()->execute();

        foreach ($data['members'] as $member) {
            unset($member['clan']);
            $player = $this->entityManager->getRepository(Player::class)->findOneBy(['playerId'=>$member['player_id']]);
            $player = $this->updatePlayer($player, $member);
            $player->setActive(true);
            $player->setRank($member['rank']);
            $player->setData([
                'profile_text' => $member['profile_text'],
                'city_name' => $member['city_name'],
                'avatar' => $member['avatar'],
                'player_id' => $member['player_id'],
                'title' => $member['title'],
                'era' => $member['era']??null,
            ]);
            $statistic = new Statistic();
            $statistic->setRawData($rawData);
            $statistic->setPlayer($player);
            $statistic->setScore($member['score']);
            $statistic->setBattles($member['won_battles']);
            $era = $this->entityManager->getRepository(Era::class)->findOneBy(['slug' => $member['era']]);
            $statistic->setEra($era);
            $statistic->getPlayer()->setEra($era);
            $this->entityManager->persist($statistic);
        }
        unset($data['members']);
        $guildStatistic = new GuildStatistic($data['points'], $data['rank'], $data['level'], $data['prestige'], $data);
        $guildStatistic->setRawData($rawData);
        $this->entityManager->persist($guildStatistic);
    }
    public function importResourcesData(RawData $rawData, $data) {
        foreach ($data['resources'] as $slug => $score) {
            $resource = $this->entityManager->getRepository(Resource::class)->findOneBy(['slug'=>$slug]);
            if(!$resource) {
                $resource = new Resource();
                $resource->setName($slug);
                $resource->setSlug($slug);
                $this->entityManager->persist($resource);
                $this->entityManager->flush($resource);
            }
            $statistic = new GuildResource();
            $statistic->setRawData($rawData);
            $statistic->setResource($resource);
            $statistic->setScore($score);
            $this->entityManager->persist($statistic);
        }
    }
    public function importResourcesLogsData(RawData $rawData, $data) {
        $last = $this->entityManager->getRepository(GuildTreasury::class)->last();
        $data = array_reverse($data);
        $counter = 0;
        foreach ($data as $d) {
            try {
                $counter++;

                $date = $this->stringToDateTime($d['createdAt'], clone $rawData->getCreatedAt());
                if($date > $last) {
                    $member = $d['player'];
                    unset($member['clan']);
                    $player = $this->entityManager->getRepository(Player::class)->findOneBy(['playerId'=>$member['player_id']]);
                    $player = $this->updatePlayer($player, $member);
                    $guildTreasury = new GuildTreasury();
                    $guildTreasury->setRawData($rawData);
                    $guildTreasury->setPlayer($player);
                    $resource = $this->entityManager->getRepository(Resource::class)->findOneBy(['slug'=>$d['resource']]);
                    $guildTreasury->setResource($resource);
                    $guildTreasury->setAmount($d['amount']);
                    $guildTreasury->setAction($d['action']);
                    $guildTreasury->setCreatedAt($date);
                    $this->entityManager->persist($guildTreasury);
                    if ($counter % 100 == 0) {
                        $this->entityManager->flush();
                    }
                }
            }
            catch (\Exception $e) {
                $this->logger->error($e->getMessage(), [
                    'item' => $d,
                ]);
            }
            $this->entityManager->flush();
        }
    }
    public function importAdventuresData(RawData $rawData, $data) {
        foreach ($data as $d) {
            $member = $d['player'];
            unset($member['clan']);
            $player = $this->entityManager->getRepository(Player::class)->findOneBy(['playerId'=>$member['player_id']]);
            $player = $this->updatePlayer($player, $member);
            $statistic = new AdventureStatistic();
            $statistic->setRawData($rawData);
            $statistic->setPlayer($player);
            if($d['solvedEncounters']) {
                $statistic->setScore($d['expeditionPoints']??0);
                $statistic->setConflicts($d['solvedEncounters']);
            }
            else {
                $statistic->setScore(0);
                $statistic->setConflicts(0);
            }
            $this->entityManager->persist($statistic);
        }
    }
    public function importBattlegroundsData(RawData $rawData, $data) {
        foreach ($data as $d) {
            $member = $d['player'];
            $player = $this->entityManager->getRepository(Player::class)->findOneBy(['playerId'=>$member['player_id']]);
            $player = $this->updatePlayer($player, $member);
            $statistic = new BattlegroundStatistic();
            $statistic->setRawData($rawData);
            $statistic->setPlayer($player);
            $points = $npoints = $bpoints = 0;
            if(isset($d['negotiationsWon'])) {
                $npoints = $d['negotiationsWon'];
                $points += $npoints*2;
            }
            if(isset($d['battlesWon'])) {
                $bpoints = $d['battlesWon'];
                $points += $bpoints;
            }
            $statistic->setScore($points);
            $statistic->setBattles($bpoints);
            $statistic->setNegotiations($npoints);
            $this->entityManager->persist($statistic);
        }
    }
    public function importBattlegroundData(RawData $rawData, $data) {
        $endsAt = new \DateTime();
        $endsAt->setTimestamp($data['endsAt']);
        $endsAt = \DateTimeImmutable::createFromMutable($endsAt);
        $battleground = $this->entityManager->getRepository(Battleground::class)->findOneBy(['endsAt' => $endsAt]);
        if(!$battleground) {
            $battleground = new Battleground();
            $battleground->setRawData($rawData);
            $battleground->setEndsAt($endsAt);
            $battleground->setMapId($data['map']['id']);
            foreach ($data['battlegroundParticipants'] as $d) {
                $participant = new BattlegroundParticipant();
                $participant->setParticipantId($d['participantId']);
                $participant->setGuildId($d['clan']['id']);
                $participant->setName($d['clan']['name']);
                $participant->setMemberSum($d['clan']['membersNum']);
                $participant->setFlag($d['clan']['flag']);
                $participant->setColor($d['colour']);
                $battleground->addParticipant($participant);
            }
            foreach ($data['map']['provinces'] as $d) {
                $province = new BattlegroundProvince();
                $province->setData($this->entityManager->getRepository(BattlegroundProvinceData::class)->findOneBy(['mapId'=>$data['map']['id'], 'provinceId'=> $d['id']??0]));
                $province->setSlots($d['totalBuildingSlots']??0);
                $province->setVictoryPoints($d['victoryPoints']);
                $province->setIsSpawnSpot($d['isSpawnSpot']??false);
                $province->setOwnerId($province->getIsSpawnSpot()?$d['ownerId']:null);
                $battleground->addProvince($province);
            }
            $this->entityManager->persist($battleground);
            $this->entityManager->flush();
        }
    }
    public function importBattlegroundProvinceData(RawData $rawData, $data) {
        $battleground = $this->entityManager->getRepository(Battleground::class)->findOneByDate($rawData->getCreatedAt());
        if($battleground && $battleground->getStartsAt()<$rawData->getCreatedAt()) {
            $province = $battleground->getProvinceById($data['provinceId']??0);
            if($province) {
                $province->setRawData($rawData);
                foreach ($data['availableBuildings'] as $d) {
                    $building = $this->entityManager->getRepository(BattlegroundBuilding::class)->findOneBy(['slug' => $d['buildingId']]);
                    if($building) {
                        $provinceBuilding = new BattlegroundProvinceBuilding();
                        $provinceBuilding->setBuilding($building);
                        foreach ($d['costs']['resources'] as $resourceSlug => $amount) {
                            $resource = $this->entityManager->getRepository(Resource::class)->findOneBy(['slug' => $resourceSlug]);
                            if($resource) {
                                $cost = new BattlegroundProvinceBuildingCost();
                                $cost->setResource($resource);
                                $cost->setAmount($amount);
                                $provinceBuilding->addCost($cost);
                            }

                        }
                        $province->addBuilding($provinceBuilding);
                    }
                }
                $this->entityManager->persist($province);
            }
            $this->entityManager->flush();
        }
    }
    public function updatePlayer($player, $member) {
        if(!$player) {
            $player = new Player();
            $player->setPlayerId($member['player_id']);
            $player->setColor(self::adjustBrightness($this->getColor($member['player_id']),100));
        }
        if($player->getName() !== $member['name']) {
            $player->setPreviousName($player->getName());
        }
        $player->setName($member['name']);
        if(isset($member['era'])) {
            $era = $this->entityManager->getRepository(Era::class)->findOneBy(['slug' => $member['era']]);
            $player->setEra($era);
        }
        $this->entityManager->persist($player);
        $this->entityManager->flush();
        return $player;
    }
    private function getColor($num) {
        $hash = md5('color_' . $num); // modify 'color' to get a different palette
        return '#'.substr(implode('',[
                hexdec(substr($hash, 0, 2)),
                hexdec(substr($hash, 2, 2)),
                hexdec(substr($hash, 4, 2))]), 0, 6);
    }
    public static function adjustBrightness($hex, $steps) {
        // Steps should be between -255 and 255. Negative = darker, positive = lighter
        $steps = max(-255, min(255, $steps));

        // Normalize into a six character long hex string
        $hex = str_replace('#', '', (string) $hex);
        if (strlen($hex) == 3) {
            $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
        }

        // Split into three parts: R, G and B
        $color_parts = str_split($hex, 2);
        $return = '#';

        foreach ($color_parts as $color) {
            $color   = hexdec($color); // Convert to decimal
            $color   = max(0,min(255,$color + $steps)); // Adjust color
            $return .= str_pad(dechex($color), 2, '0', STR_PAD_LEFT); // Make two char hex code
        }

        return $return;
    }
    public static function hex2rgba($color, $opacity = false) {

        $default = 'rgb(0,0,0)';

        //Return default if no color provided
        if(empty($color))
            return $default;

        //Sanitize $color if "#" is provided
        if ($color[0] == '#' ) {
            $color = substr( (string) $color, 1 );
        }

        //Check if color has 6 or 3 characters and get values
        if (strlen((string) $color) == 6) {
            $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        } elseif ( strlen( (string) $color ) == 3 ) {
            $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        } else {
            return $default;
        }

        //Convert hexadec to rgb
        $rgb =  array_map('hexdec', $hex);

        //Check if opacity is set(rgba or rgb)
        if($opacity){
            if(abs($opacity) > 1)
                $opacity = 1.0;
            $output = 'rgba('.implode(",",$rgb).','.$opacity.')';
        } else {
            $output = 'rgb('.implode(",",$rgb).')';
        }

        //Return rgb(a) color string
        return $output;
    }
    /**
     * @param Header[] $headers
     * @param $name
     */
    private function getHeader(array $headers, $name): ?string {
        foreach ($headers as $header) {
            if(strtolower($header->getName()) === strtolower((string) $name)) {
                return $header->getValue();
            }
        }
        return null;
    }
    public function stringToDateTime(string $date, \DateTime $currentDate): ?\DateTime {

        $weekDayPlToEn = [
            'Poniedziałek' => 1,
            'Wtorek' => 2,
            'Środa' => 3,
            'Czwartek' => 4,
            'Piątek' => 5,
            'Sobota' => 6,
            'Niedziela' => 7,
        ];
        preg_match('/dzisiaj o (\d+):(\d+)/i', $date, $output);
        if(count($output)) {
            $currentDate->setTime((int)$output[1], (int)$output[2]);
            return $currentDate;
        }
        preg_match('/wczoraj o (\d+):(\d+)/i', $date, $output);
        if(count($output)) {
            $currentDate->sub( new \DateInterval('P1D')) ;
            $currentDate->setTime((int)$output[1], (int)$output[2]);
            return $currentDate;
        }
        foreach ($weekDayPlToEn as $pl => $en) {
            preg_match('/('.$pl.') o (\d+):(\d+)/i', $date, $output);
            if(count($output)) {
                $currentWeekDay = (int)$currentDate->format('N');
                if($currentWeekDay>$en) {
                    $days = $currentWeekDay-$en;
                }
                else {
                    $days = 7-$en+(int)$currentDate->format('N');
                }
                $currentDate->sub( new \DateInterval('P'.$days.'D')) ;
                $currentDate->setTime((int)$output[1], (int)$output[2]);
                return $currentDate;
            }
        }
        preg_match('/(\d{2})\.(\d{2})\.(\d{4}) o (\d+):(\d+)/', $date, $output);
        if(count($output)) {
            $currentDate->setDate((int)$output[3], (int)$output[2], (int)$output[1]);
            $currentDate->setTime((int)$output[4], (int)$output[5]);
            return $currentDate;
        }
        return null;
    }

}