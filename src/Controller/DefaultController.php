<?php
namespace App\Controller;

use App\Entity\Battleground;
use App\Entity\BattlegroundBuilding;
use App\Entity\BattlegroundProvinceBuilding;
use App\Entity\BattlegroundProvinceBuildingCost;
use App\Entity\BattlegroundProvinceData;
use App\Entity\GuildResource;
use App\Entity\GuildTreasury;
use App\Entity\OtherGuildStats;
use App\Entity\Page;
use App\Entity\Player;
use App\Entity\RawData;
use App\Entity\Resource;
use App\Entity\Statistic;
use App\Form\ImportHarType;
use App\Form\SearchAdventuresType;
use App\Form\SearchBattlegroundsType;
use App\Form\SearchBuildingsCostType;
use App\Form\SearchGuildTreasuryType;
use App\Form\SearchResourcesType;
use App\Form\SearchStatisticsType;
use App\Handler\RawDataHandler;
use App\Handler\StatisticHandler;
use Deviantintegral\Har\Repository\HarFileRepository;
use Doctrine\ORM\EntityManagerInterface;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\String\ByteString;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

class DefaultController extends AbstractController
{

    public function __construct(private EntityManagerInterface $em)
    {
    }

    #[Route(path: '/', name: 'homepage')]
    public function index(Request $request, EntityManagerInterface $entityManager, StatisticHandler $statisticHandler, ChartBuilderInterface $chartBuilder): Response
    {
        $data = [
            'current' => true,
            'dateFrom' => new \DateTime('-2 month'),
            'dateTo' => new \DateTime(),
            'sort' => 'points',
        ];
        $form = $this->createForm(SearchStatisticsType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
        }

        $statistics = $entityManager->getRepository(RawData::class)->byWeek(RawData::TYPE_STATISTICS, $data);

        $chart = $chartBuilder->createChart(Chart::TYPE_LINE);
        $preparedData = $statisticHandler->preparePlayerData($statistics, $data);
        $chart->setData($statisticHandler->parseChartData($preparedData));
        $chart->setOptions($statisticHandler->getChartOptions());

        return $this->render('default/index.html.twig', [
            'form' => $form->createView(),
            'data' => $preparedData,
            'chart' => $chart,
        ]);
    }
    #[Route(path: '/battles', name: 'battles')]
    public function battles(Request $request, EntityManagerInterface $entityManager, StatisticHandler $statisticHandler, ChartBuilderInterface $chartBuilder): Response
    {
        $data = [
            'current' => true,
            'dateFrom' => new \DateTime('-2 month'),
            'dateTo' => new \DateTime(),
            'sort' => 'points',
        ];
        $form = $this->createForm(SearchStatisticsType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
        }

        $statistics = $entityManager->getRepository(RawData::class)->byWeek(RawData::TYPE_STATISTICS, $data);

        $chart = $chartBuilder->createChart(Chart::TYPE_LINE);
        $preparedData = $statisticHandler->preparePlayerData($statistics, $data, true);
        $chart->setData($statisticHandler->parseChartData($preparedData));
        $chart->setOptions($statisticHandler->getChartOptions());

        return $this->render('default/index.html.twig', [
            'form' => $form->createView(),
            'data' => $preparedData,
            'chart' => $chart,
        ]);
    }
    #[Route(path: '/resources', name: 'page_resources')]
    public function resources(Request $request, EntityManagerInterface $entityManager, StatisticHandler $statisticHandler, ChartBuilderInterface $chartBuilder): Response
    {
        $data = [
            'dateFrom' => new \DateTime('-2 month'),
            'dateTo' => new \DateTime(),
        ];
        $form = $this->createForm(SearchResourcesType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
        }
        $statistics = $entityManager->getRepository(RawData::class)->byWeek(RawData::TYPE_RESOURCES, $data);
        $chart = $chartBuilder->createChart(Chart::TYPE_LINE);
        $preparedData = $statisticHandler->prepareGuildResourceData($statistics);
        $chart->setData($statisticHandler->parseChartData($preparedData));

        $chart->setOptions($statisticHandler->getChartOptions());
        return $this->render('default/resources.html.twig', [
            'form' => $form->createView(),
            'data' => $preparedData,
            'resource' => new Resource(),
            'chart' => $chart,
        ]);
    }
    #[Route(path: '/resources_list', name: 'page_resources_list')]
    public function resources_list(Request $request, EntityManagerInterface $em, StatisticHandler $statisticHandler, ChartBuilderInterface $chartBuilder): Response
    {
        $data = [
            'dateFrom' => new \DateTime('-2 years'),
            'dateTo' => new \DateTime(),
        ];
        $form = $this->createForm(SearchGuildTreasuryType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
        }
        $qb = $em->getRepository(GuildTreasury::class)->listQb($data);
        $pagerfanta = new Pagerfanta(new QueryAdapter($qb));
        $pagerfanta->setMaxPerPage(50);
        $pagerfanta->setCurrentPage($request->query->get('page', 1));
        return $this->render('default/resources_list.html.twig', [
            'form' => $form->createView(),
            'guildTreasuries' => $pagerfanta,
        ]);
    }
    #[Route(path: '/resources_bp', name: 'page_resources_building_production')]
    public function resources_building_production(Request $request, EntityManagerInterface $em, StatisticHandler $statisticHandler, ChartBuilderInterface $chartBuilder): Response
    {
        $data = [
            'dateFrom' => new \DateTime('-2 month'),
            'dateTo' => new \DateTime(),
            'groupBy' => 'week',
        ];
        $form = $this->createForm(SearchResourcesType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
        }
        return $this->render('default/resources_building_production.html.twig', [
            'form' => $form->createView(),
            'productions' => $em->getRepository(GuildTreasury::class)->groupedByResource([GuildTreasury::ACTION_PRODUCTION], $data),
        ]);
    }
    #[Route(path: '/resources_detail', name: 'page_resources_detail')]
    public function resources_detail(Request $request, EntityManagerInterface $entityManager, StatisticHandler $statisticHandler, ChartBuilderInterface $chartBuilder): Response
    {
        $data = [
            'dateFrom' => new \DateTime('-2 weeks'),
            'dateTo' => new \DateTime(),
        ];
        $form = $this->createForm(SearchResourcesType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
        }

        return $this->render('default/resources_detail.html.twig', [
            'form' => $form->createView(),
            'dataLogs' => $entityManager->getRepository(GuildTreasury::class)->groupedByAge($data),
            'dataLogsUsers' => $entityManager->getRepository(GuildTreasury::class)->groupedByPlayer($data),
            'resource' => new Resource(),
        ]);
    }
    #[Route(path: '/resources_gpc', name: 'page_resources_gpc')]
    public function resources_gpc(Request $request, EntityManagerInterface $entityManager, StatisticHandler $statisticHandler, ChartBuilderInterface $chartBuilder): Response
    {
        $gpcDates = $this->getGpcDates();
        $lastGpc = end($gpcDates);
        $data = [
            'dateFrom' => $lastGpc?$lastGpc['dateFrom']: new \DateTime('-2 weeks'),
            'dateTo' => $lastGpc?$lastGpc['dateTo']: new \DateTime(),
            'gpc' => true
        ];
        $form = $this->createForm(SearchResourcesType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
        }

        $battlegrounds = $entityManager->getRepository(RawData::class)->getByCriteria(RawData::TYPE_BATTLEGROUNDS, $data);
        $battleground = end($battlegrounds);

        return $this->render('default/resources_gpc.html.twig', [
            'form' => $form->createView(),
            'dataLogs' => $entityManager->getRepository(GuildTreasury::class)->groupedByAge($data),
            'dataLogsUsers' => $entityManager->getRepository(GuildTreasury::class)->groupedByPlayer($data),
            'resource' => new Resource(),
            'battleground' => $battleground,
            'gpcs' => array_reverse($gpcDates),
        ]);
    }
    #[Route(path: '/resources_gpc_cost', name: 'page_resources_gpc_cost')]
    public function resources_gpc_cost(Request $request, EntityManagerInterface $entityManager, StatisticHandler $statisticHandler, ChartBuilderInterface $chartBuilder): Response
    {
        $gpcDates = $this->getGpcDates();
        $lastGpc = end($gpcDates);
        $data = [
            'dateFrom' => $lastGpc?$lastGpc['dateFrom']: new \DateTime('-2 weeks'),
            'dateTo' => $lastGpc?$lastGpc['dateTo']: new \DateTime(),
            'gpc' => true
        ];
        $form = $this->createForm(SearchResourcesType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
        }

        $battlegrounds = $entityManager->getRepository(RawData::class)->getByCriteria(RawData::TYPE_BATTLEGROUNDS, $data);
        $battleground = end($battlegrounds);

        return $this->render('default/resources_gpc_cost.html.twig', [
            'form' => $form->createView(),
            'dataLogs' => $entityManager->getRepository(GuildTreasury::class)->groupedByAgeCostGPC($data),
            'resource' => new Resource(),
            'battleground' => $battleground,
            'gpcs' => array_reverse($gpcDates),
        ]);
    }
    #[Route(path: '/battleground/{id}/map', name: 'battleground_map')]
    public function battleground_map(StatisticHandler $statisticHandler, SerializerInterface $serializer, Battleground $battleground): Response
    {
        $preparedData = $statisticHandler->prepareBattlegroundData($battleground);
        return $this->render('default/battleground_map.html.twig', [
            'battleground' => $preparedData,
            'battlegroundJson' => $serializer->serialize($preparedData, 'json', ['json_encode_options' => \JSON_PRESERVE_ZERO_FRACTION, 'groups' => 'json']),
        ]);
    }
    #[Route(path: '/battleground/{id}/buildings_cost', name: 'battleground_buildings_cost')]
    public function battleground_buildings_cost(Request $request, EntityManagerInterface $em, Battleground $battleground): Response
    {
        $data = [
            'battleground' => $battleground,
            'dateFrom' => $battleground->getStartsAt(),
            'dateTo' => $battleground->getEndsAt(),
        ];
        $form = $this->createForm(SearchBuildingsCostType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
        }
        $eras = $em->getRepository(BattlegroundProvinceBuildingCost::class)->list($data);
        return $this->render('default/battleground_buildings_cost.html.twig', [
            'form' => $form->createView(),
            'eras' => $eras,
        ]);
    }
    #[Route(path: '/battleground/{id}/buildings_test', name: 'battleground_buildings_test')]
    public function battleground_buildings_test(EntityManagerInterface $em, Battleground $battleground): Response
    {
        $guildTsQb= $em->getRepository(GuildTreasury::class)->createQueryBuilder('e');
        $guildTsQb->select('e.createdAt')
            ->andWhere($guildTsQb->expr()->between('e.createdAt', ':fromDate', ':toDate'))
            ->andWhere($guildTsQb->expr()->eq('e.action', ':action'))
            ->setParameter('fromDate', $battleground->getStartsAt()->format('Y-m-d 00:00:00'))
            ->setParameter('toDate', $battleground->getEndsAt()->format('Y-m-d 23:59:59'))
            ->setParameter('action', GuildTreasury::ACTION_GPC_BUILDING)
            ->addGroupBy('e.createdAt');
        $guildTsDates = $guildTsQb->getQuery()->getResult();
        $guildTsRQb= $em->getRepository(GuildTreasury::class)->createQueryBuilder('e');
        $guildTsRQb->select("CONCAT_WS('-', date_format(e.createdAt,'%Y%m%d%H%i'), e.amount, identity(e.resource)) AS uniq, identity(e.player) as player")
            ->andWhere($guildTsQb->expr()->between('e.createdAt', ':fromDate', ':toDate'))
            ->andWhere($guildTsQb->expr()->eq('e.action', ':action'))
            ->setParameter('fromDate', $battleground->getStartsAt()->format('Y-m-d 00:00:00'))
            ->setParameter('toDate', $battleground->getEndsAt()->format('Y-m-d 23:59:59'))
            ->setParameter('action', GuildTreasury::ACTION_GPC_BUILDING);
        $guildTsRQb = $guildTsRQb->getQuery()->getArrayResult();
        $guildTsRQb = array_combine(array_column($guildTsRQb, 'uniq'), array_column($guildTsRQb, 'player'));
        $buildedBuildings = [];
        foreach ($guildTsDates as $guildTsDate) {
            foreach ($battleground->getProvinces() as $province) {
                if($province->getSlots() < 1) continue;
                foreach ($province->getBuildings() as $building) {
                    $c = $building->getCosts()->count();
                    $cf = 0;
                    $playerId = null;
                    foreach ($building->getCosts() as $cost) {
                        $key = $guildTsDate['createdAt']->format('YmdHi').'--'.$cost->getAmount().'-'.$cost->getResource()->getId();
                        if (isset($guildTsRQb[$key])) {
                            $playerId = $guildTsRQb[$key];
                            $cf+=1;
                        }
                    }
                    if($cf===$c) {
                        dump(1);
                        $buildedBuildings[] = [
                            'building' => $building,
                            'player' => $em->getRepository(Player::class)->find($playerId),
                        ];
                    }

                }
            }
            break;
        }
        dd($buildedBuildings);
        return $this->render('base.html.twig');
    }
    #[Route(path: '/resources_gvg', name: 'resources_gvg')]
    public function resources_gvg(Request $request, EntityManagerInterface $entityManager, StatisticHandler $statisticHandler, ChartBuilderInterface $chartBuilder): Response
    {
        $data = [
            'dateFrom' => new \DateTime('-2 weeks'),
            'dateTo' => new \DateTime(),
        ];
        $form = $this->createForm(SearchResourcesType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
        }

        return $this->render('default/resources_gvg.html.twig', [
            'form' => $form->createView(),
            'oblegiDataLogsUsers' => $entityManager->getRepository(GuildTreasury::class)->oblegiGroupedByPlayer($data),
        ]);
    }
    #[Route(path: '/adventures', name: 'page_adventures')]
    public function adventures(Request $request, EntityManagerInterface $entityManager, StatisticHandler $statisticHandler, ChartBuilderInterface $chartBuilder): Response
    {
        $data = [
            'dateFrom' => new \DateTime('-2 month'),
            'dateTo' => new \DateTime(),
            'conflicts' => 64,
        ];
        $form = $this->createForm(SearchAdventuresType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
        }
        $statistics = $entityManager->getRepository(RawData::class)->byWeek(RawData::TYPE_ADVENTURES, $data);

        $chart = $chartBuilder->createChart(Chart::TYPE_LINE);
        $preparedData = $statisticHandler->prepareAdventuresPlayerData($statistics, $data);
        $chart->setData($statisticHandler->parseChartData($preparedData));
        $chart->setOptions($statisticHandler->getChartOptions());

        return $this->render('default/adventures.html.twig', [
            'form' => $form->createView(),
            'data' => $preparedData,
            'chart' => $chart,
        ]);
    }
    #[Route(path: '/battlegrounds', name: 'page_battlegrounds')]
    public function battlegrounds(Request $request, EntityManagerInterface $entityManager, StatisticHandler $statisticHandler, ChartBuilderInterface $chartBuilder): Response
    {
        $data = [
            'dateFrom' => new \DateTime('-2 month'),
            'dateTo' => new \DateTime(),
            'points' => 10000,
        ];
        $form = $this->createForm(SearchBattlegroundsType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
        }
        $statistics = $entityManager->getRepository(RawData::class)->byWeek(RawData::TYPE_BATTLEGROUNDS, $data);
        $chart = $chartBuilder->createChart(Chart::TYPE_LINE);
        $preparedData = $statisticHandler->prepareBattlegroundsData($statistics, $data);
        $chart->setData($statisticHandler->parseChartData($preparedData));
        $chart->setOptions($statisticHandler->getChartOptions());
        return $this->render('default/battlegrounds.html.twig', [
            'form' => $form->createView(),
            'data' => $preparedData,
            'chart' => $chart,
        ]);
    }
    #[Route(path: '/battlegrounds/{id}', name: 'page_battlegrounds_detail')]
    public function battlegrounds_detail(EntityManagerInterface $entityManager, StatisticHandler $statisticHandler, $id): Response
    {
        $data = [
            'dateFrom' => new \DateTime('-2 month'),
            'dateTo' => new \DateTime(),
            'points' => 10000,
        ];
        $statistics = $entityManager->getRepository(RawData::class)->findOneBy(['id'=>$id, 'type' => RawData::TYPE_BATTLEGROUNDS]);
        if(!$statistics) {
            throw $this->createNotFoundException();
        }
        $preparedData = $statisticHandler->prepareBattlegroundsData([$statistics], $data);
        return $this->render('default/battlegrounds_detail.html.twig', [
            'stats' => $statistics,
            'data' => $preparedData,
        ]);
    }
    #[Route(path: '/guild', name: 'page_guild')]
    public function guild(EntityManagerInterface $entityManager, StatisticHandler $statisticHandler): Response
    {
        $statistics = $entityManager->getRepository(RawData::class)->findBy(['type' => RawData::TYPE_STATISTICS],['createdAt'=>'ASC'], 99999);
        return $this->render('default/guild.html.twig', [
            'data' => $statisticHandler->prepareGuildData($statistics),
        ]);
    }
    #[Route(path: '/players', name: 'players')]
    public function players(EntityManagerInterface $em): Response
    {
        $players = $em->getRepository(Player::class)->findBy(['active' => true]);
        $eras = $destinationEras =  $allEras = [];
        foreach($players as $player)
        {
            $eras[$player->getEra()->getNumber()][] = $player;
            $destinationEras[$player->getDestinationEra()?$player->getDestinationEra()->getNumber():$player->getEra()->getNumber()][] = $player;
            if($player->getDestinationEra()) {
                $allEras[$player->getDestinationEra()->getNumber()][] = $player;
            }
            $allEras[$player->getEra()->getNumber()][] = $player;
        }
        krsort($allEras);
        krsort($eras);
        krsort($destinationEras);
        return $this->render('default/players.html.twig', [
            'allEras' => $allEras,
            'eras' => $eras,
            'destinationEras' => $destinationEras,
            'sum' => count($players)
        ]);
    }
    #[Route(path: '/players/{id}', name: 'players_view')]
    public function players_view(Request $request, EntityManagerInterface $em, StatisticHandler $statisticHandler, $id): Response
    {
        $data = [
            'dateFrom' => new \DateTime('-2 month'),
            'dateTo' => new \DateTime(),
            'groupBy' => 'week',
        ];
        $form = $this->createForm(SearchResourcesType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
        }
        $player = $em->getRepository(Player::class)->find($id);
        if(!$player) {
            throw $this->createNotFoundException();
        }

        $statistics = $em->getRepository(RawData::class)->byWeek(RawData::TYPE_STATISTICS, $data);
        $preparedData = $statisticHandler->preparePlayerDetailData($statistics, $data, $player);

        return $this->render('default/players_view.html.twig', [
            'form' => $form->createView(),
            'player' => $player,
            'resource' => new Resource(),
            'stats' => $preparedData,
            'dataLogs' => $em->getRepository(GuildTreasury::class)->groupedByAge($data, $player),
            'productions' => $em->getRepository(GuildTreasury::class)->groupedByResource([GuildTreasury::ACTION_PRODUCTION], $data, $player),
            'gpcs' => $this->getGpcDates(),
        ]);
    }
    #[Route(path: '/other_guild_stats', name: 'other_guild_stats')]
    public function other_guild_stats(Request $request, EntityManagerInterface $em): Response
    {
        $qb = $em->getRepository(OtherGuildStats::class)->createQueryBuilder('e')->orderBy('e.id', 'DESC');
        $pagerfanta = new Pagerfanta(new QueryAdapter($qb));
        $pagerfanta->setMaxPerPage(50);
        $pagerfanta->setCurrentPage($request->query->get('page', 1));
        return $this->render('default/other_guild_stats.html.twig', [
            'stats' => $pagerfanta,
        ]);
    }
    #[Route(path: '/other_guild_stats/{id}', name: 'other_guild_stats_view')]
    public function other_guild_stats_view(EntityManagerInterface $em, StatisticHandler $statisticHandler, $id): Response
    {
        $otherGuildStats = $em->getRepository(OtherGuildStats::class)->find($id);
        if(!$otherGuildStats) {
            throw $this->createNotFoundException();
        }
        $preparedData = $statisticHandler->prepareOtherGuildStats($otherGuildStats);

        return $this->render('default/other_guild_stats_view.html.twig', [
            'stat' => $preparedData,
        ]);
    }
    #[Route(path: '/import', name: 'import')]
    public function import(Request $request, RawDataHandler $rawDataHandler, EntityManagerInterface $entityManager, $projectDir): Response
    {
        $form = $this->createForm(ImportHarType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dir = $projectDir.'/var/har';
            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form->get('file')->getData();
            $filename = ByteString::fromRandom(8).'_'.$uploadedFile->getClientOriginalName();
            $uploadedFile->move($dir, $filename);

            $filesystem = new Filesystem();
            $harRepository = new HarFileRepository($dir);
            foreach ($harRepository->getIds() as $id) {
                $har = $harRepository->load($id);
                $rawData = $rawDataHandler->importHAR($har, $id);
                foreach ($rawData as $type => $rawDatum) {

                    $this->addFlash('success', $id.': '.$type);
                }
            }
            $filesystem->rename($dir.'/'.$filename,$dir.'_imported/'.$filename);
            return $this->redirectToRoute('import_process');
        }

        return $this->render('default/import.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    #[Route(path: '/import_process', name: 'import_process')]
    public function import_process(Request $request, RawDataHandler $rawDataHandler): Response
    {
        $form = $this->createForm(ImportHarType::class);
        if($request->query->getBoolean('start')) {
            $rawDataHandler->import();
        }
        return $this->render('default/import_process.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    #[Route(path: '/import_update', name: 'import_update')]
    public function import_update(RawDataHandler $rawDataHandler): Response
    {
        $form = $this->createForm(ImportHarType::class);
        $rawDataHandler->importUpdate();
        return $this->render('default/import.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    #[Route(path: '/custom_view', name: 'custom_view')]
    public function custom_view(Request $request, EntityManagerInterface $em): Response
    {
        try {
            $url = $request->query->get('url');
            if (!filter_var($url, FILTER_VALIDATE_URL)) {
                return new Response(hex2bin('89504e470d0a1a0a0000000d494844520000000100000001010300000025db56ca00000003504c5445000000a77a3dda0000000174524e530040e6d8660000000a4944415408d76360000000020001e221bc330000000049454e44ae426082'), Response::HTTP_OK, ['Content-Type' =>'image/png']);
            }
            $query = "INSERT INTO stat (`url`, `ip`, `date`, `time`, `amount`) values(?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE amount = amount+1, `date` = ?";

            $em->getConnection()->executeQuery($query, [
                $url,
                $request->getClientIp(),
                (new \DateTime())->format('Y-m-d'),
                (new \DateTime())->format('H:i:s'),
                1,
                (new \DateTime())->format('H:i:s'),
            ]);
        }
        catch (\Exception) {
        }
        return new Response(hex2bin('89504e470d0a1a0a0000000d494844520000000100000001010300000025db56ca00000003504c5445000000a77a3dda0000000174524e530040e6d8660000000a4944415408d76360000000020001e221bc330000000049454e44ae426082'), Response::HTTP_OK, ['Content-Type' =>'image/png']);
    }

    #[Route(path: '/s/{slug}', name: 'page')]
    public function page(EntityManagerInterface $em, $slug): Response
    {
        $entity = $em->getRepository(Page::class)->findOneBy(['slug' => $slug]);

        if(!$entity) {
            throw new NotFoundHttpException('not found');
        }

        return $this->render('default/page.html.twig', [
            'entity' => $entity,
        ]);
    }
    private function getGpcDates(): array {

        $gpcs = [];
        $begin = new \DateTime( '2021-11-22');
        $end = new \DateTime();
        $end->setTime(23,59,59);

        $interval = new \DateInterval('P14D');
        $daterange = new \DatePeriod($begin, $interval ,$end);

        foreach($daterange as $date){
            /** @var \DateTime $from */
            $from = clone $date;
            $from->add(new \DateInterval('P3D'));
            /** @var \DateTime $to */
            $to = clone $date;
            $to->add(new \DateInterval('P14D'));
            $to->setTime(8,0,0);
            if($from>new \DateTime()) {
                continue;
            }
            $gpcs[] = [

                'dateFrom' => $from,
                'dateTo' => $to,
                'battleground' => $this->em->getRepository(Battleground::class)->findOneBy(['endsAt' => \DateTimeImmutable::createFromMutable($to)]),
            ];
        }
        return $gpcs;
    }
    #[Route(path: '/dk', name: 'dk')]
    public function dk(RawDataHandler $dataHandler): Response
    {
        return $this->render('default/dk.html.twig');
    }
    /**
     * @Route("/u", name="u")
     */
    public function u(EntityManagerInterface $em)
    {
        $qb = $em->getRepository(RawData::class)->createQueryBuilder('e');
        $data = $qb
            ->andWhere($qb->expr()->eq('e.type', ':type'))
            ->setParameter('type', RawData::TYPE_RESOURCES_LOGS)
            ->andWhere($qb->expr()->like('e.createdAt', ':createdAt'))
            ->setParameter('createdAt', '2022-06%')
            ->setMaxResults(1)
            ->setFirstResult(18)
            ->getQuery()
            ->getResult();
        foreach ($data as $datum) {
            echo json_encode($datum->getCreatedAt());
            echo json_encode($datum->getData());
            exit;
        }
        return $this->render('base.html.twig');
    }
}