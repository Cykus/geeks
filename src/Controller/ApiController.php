<?php

namespace App\Controller;

use App\Entity\OtherGuildStats;
use App\Form\LoginType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route(path: '/api', name: 'api')]
class ApiController extends AbstractController
{
    #[Route(path: '/battleData', name: 'battleData')]
    public function battleData(Request $request, EntityManagerInterface $em): Response
    {
        $data = json_decode($request->getContent(), true);
        $hash = md5($request->getContent());
        $otherGuildStats = $em->getRepository(OtherGuildStats::class)->findOneBy(['hash' => $hash]);
        if(!$otherGuildStats)
        $otherGuildStats = New OtherGuildStats();
        $otherGuildStats->setStats($data);
        $otherGuildStats->setHash($hash);
        $em->persist($otherGuildStats);
        $em->flush();

        return $this->json(['id' => $otherGuildStats->getId(), 'url' => $this->generateUrl('other_guild_stats_view', ['id' => $otherGuildStats->getId()], UrlGeneratorInterface::ABSOLUTE_URL)]);
    }
}
