<?php

namespace App\Entity;

use App\Repository\BattlegroundProvinceBuildingCostRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: BattlegroundProvinceBuildingCostRepository::class)]
class BattlegroundProvinceBuildingCost
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups('json')]
    #[ORM\Column(type: 'integer')]
    private $amount;

    #[Groups('json')]
    #[ORM\ManyToOne(targetEntity: Resource::class)]
    #[ORM\JoinColumn(nullable: false)]
    private $resource;

    #[ORM\ManyToOne(targetEntity: BattlegroundProvinceBuilding::class, inversedBy: 'costs')]
    #[ORM\JoinColumn(nullable: false)]
    private $building;

    #[Groups('json')]
    private array $computedData = [];


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getResource(): ?Resource
    {
        return $this->resource;
    }

    public function setResource(?Resource $resource): self
    {
        $this->resource = $resource;

        return $this;
    }

    public function getBuilding(): ?BattlegroundProvinceBuilding
    {
        return $this->building;
    }

    public function setBuilding(?BattlegroundProvinceBuilding $building): self
    {
        $this->building = $building;

        return $this;
    }

    public function getComputedData(): array
    {
        return $this->computedData;
    }

    public function setComputedData(array $computedData): self
    {
        $this->computedData = $computedData;
        return $this;
    }
}
