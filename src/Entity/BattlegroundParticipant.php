<?php

namespace App\Entity;

use App\Repository\BattlegroundParticipantRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: BattlegroundParticipantRepository::class)]
class BattlegroundParticipant
{
    #[Groups('json')]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups('json')]
    #[ORM\Column(type: 'integer')]
    private $participantId;

    #[Groups('json')]
    #[ORM\Column(type: 'integer')]
    private $guildId;

    #[Groups('json')]
    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[Groups('json')]
    #[ORM\Column(type: 'integer')]
    private $memberSum;

    #[ORM\ManyToOne(targetEntity: Battleground::class, inversedBy: 'participants')]
    #[ORM\JoinColumn(nullable: false)]
    private $battleground;

    #[Groups('json')]
    #[ORM\Column(type: 'string', length: 255)]
    private $flag;

    #[Groups('json')]
    #[ORM\Column(type: 'string', length: 255)]
    private $color;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getParticipantId(): ?int
    {
        return $this->participantId;
    }

    public function setParticipantId(int $participantId): self
    {
        $this->participantId = $participantId;

        return $this;
    }

    public function getGuildId(): ?int
    {
        return $this->guildId;
    }

    public function setGuildId(int $guildId): self
    {
        $this->guildId = $guildId;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMemberSum(): ?int
    {
        return $this->memberSum;
    }

    public function setMemberSum(int $memberSum): self
    {
        $this->memberSum = $memberSum;

        return $this;
    }

    public function getBattleground(): ?Battleground
    {
        return $this->battleground;
    }

    public function setBattleground(?Battleground $battleground): self
    {
        $this->battleground = $battleground;

        return $this;
    }

    public function getFlag(): ?string
    {
        return $this->flag;
    }

    public function setFlag(string $flag): self
    {
        $this->flag = $flag;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }
}
