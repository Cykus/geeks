<?php

namespace App\Entity;

use App\Repository\GuildTreasuryRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
#[ORM\Entity(repositoryClass: GuildTreasuryRepository::class)]
class GuildTreasury
{
    final const ACTION_GVG_SECTOR = 'Kontynent gildii: odblokowano pole';
    final const ACTION_GVG_REMOVE_SECTOR = 'Kontynent gildii: uwolnij';
    final const ACTION_GPC_BUILDING = 'Pole chwały: Umieść budynek';
    final const ACTION_PRODUCTION = 'Produkcja budynku';
    final const ACTION_GVG_ARMY = 'Rozmieszczenie armii oblężniczej';
    final const ACTION_TREASURY = 'Wpłata do skarbca gildii';
    final const ACTION_WG = 'Wyprawa gildii: odblokowano poziom trudności';
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 100)]
    private $action;

    #[ORM\Column(type: 'integer')]
    private $amount;

    #[ORM\Column(type: 'datetime')]
    private $createdAt;

    #[ORM\ManyToOne(targetEntity: Player::class, inversedBy: 'guildTreasuries')]
    #[ORM\JoinColumn(nullable: false)]
    private $player;

    #[ORM\ManyToOne(targetEntity: Resource::class, inversedBy: 'guildTreasuries')]
    #[ORM\JoinColumn(nullable: false)]
    private $resource;

    #[ORM\ManyToOne(targetEntity: RawData::class, inversedBy: 'guildTreasuries')]
    #[ORM\JoinColumn(nullable: false)]
    private $rawData;

    public static function getActionChoices(): array {
        return [
            self::ACTION_GVG_SECTOR => self::ACTION_GVG_SECTOR,
            self::ACTION_GVG_REMOVE_SECTOR => self::ACTION_GVG_REMOVE_SECTOR,
            self::ACTION_GPC_BUILDING => self::ACTION_GPC_BUILDING,
            self::ACTION_PRODUCTION => self::ACTION_PRODUCTION,
            self::ACTION_GVG_ARMY => self::ACTION_GVG_ARMY,
            self::ACTION_TREASURY => self::ACTION_TREASURY,
            self::ACTION_WG => self::ACTION_WG,
        ];
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAction(): ?string
    {
        return $this->action;
    }

    public function setAction(string $action): self
    {
        $this->action = $action;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getPlayer(): ?Player
    {
        return $this->player;
    }

    public function setPlayer(?Player $player): self
    {
        $this->player = $player;

        return $this;
    }

    public function getResource(): ?Resource
    {
        return $this->resource;
    }

    public function setResource(?Resource $resource): self
    {
        $this->resource = $resource;

        return $this;
    }

    public function getRawData(): ?RawData
    {
        return $this->rawData;
    }

    public function setRawData(?RawData $rawData): self
    {
        $this->rawData = $rawData;

        return $this;
    }
}
