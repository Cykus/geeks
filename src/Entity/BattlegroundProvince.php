<?php

namespace App\Entity;

use App\Repository\BattlegroundProvinceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: BattlegroundProvinceRepository::class)]
class BattlegroundProvince
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Battleground::class, inversedBy: 'provinces')]
    #[ORM\JoinColumn(nullable: false)]
    private $battleground;

    #[Groups('json')]
    #[ORM\Column(type: 'integer')]
    private $victoryPoints;

    #[Groups('json')]
    #[ORM\Column(type: 'integer', nullable: true)]
    private $slots;

    #[Groups('json')]
    #[ORM\OneToMany(targetEntity: BattlegroundProvinceBuilding::class, mappedBy: 'province', cascade: ['all'], orphanRemoval: true)]
    private $buildings;

    #[Groups('json')]
    #[ORM\Column(type: 'boolean')]
    private bool $isSpawnSpot = false;

    #[Groups('json')]
    #[ORM\ManyToOne(targetEntity: BattlegroundProvinceData::class, inversedBy: 'provinces')]
    #[ORM\JoinColumn(nullable: false)]
    private $data;

    #[Groups('json')]
    #[ORM\Column(type: 'integer', nullable: true)]
    private $ownerId;

    #[Groups('json')]
    private array $computedData = [];

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?RawData $rawData = null;

    public function __construct()
    {
        $this->buildings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBattleground(): ?Battleground
    {
        return $this->battleground;
    }

    public function setBattleground(?Battleground $battleground): self
    {
        $this->battleground = $battleground;

        return $this;
    }

    public function getVictoryPoints(): ?int
    {
        return $this->victoryPoints;
    }

    public function setVictoryPoints(int $victoryPoints): self
    {
        $this->victoryPoints = $victoryPoints;

        return $this;
    }

    public function getSlots(): ?int
    {
        return $this->slots;
    }

    public function setSlots(?int $slots): self
    {
        $this->slots = $slots;

        return $this;
    }

    public function getBuildingByType(string $slug): ?BattlegroundProvinceBuilding
    {
        return $this->getBuildings()->filter(
            fn($entry) => $entry->getBuilding()->getSlug() === $slug
        )->first()?:null;
    }
    /**
     * @return Collection|BattlegroundProvinceBuilding[]
     */
    public function getBuildings(): Collection
    {
        return $this->buildings;
    }

    public function addBuilding(BattlegroundProvinceBuilding $building): self
    {
        if (!$this->buildings->contains($building)) {
            $this->buildings[] = $building;
            $building->setProvince($this);
        }

        return $this;
    }

    public function removeBuilding(BattlegroundProvinceBuilding $building): self
    {
        if ($this->buildings->removeElement($building)) {
            // set the owning side to null (unless already changed)
            if ($building->getProvince() === $this) {
                $building->setProvince(null);
            }
        }

        return $this;
    }

    public function getIsSpawnSpot(): ?bool
    {
        return $this->isSpawnSpot;
    }

    public function setIsSpawnSpot(bool $isSpawnSpot): self
    {
        $this->isSpawnSpot = $isSpawnSpot;

        return $this;
    }

    public function getData(): ?BattlegroundProvinceData
    {
        return $this->data;
    }

    public function setData(?BattlegroundProvinceData $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getOwnerId(): ?int
    {
        return $this->ownerId;
    }

    public function setOwnerId(?int $ownerId): self
    {
        $this->ownerId = $ownerId;

        return $this;
    }

    public function getComputedData(): array
    {
        return $this->computedData;
    }

    public function setComputedData(array $computedData): self
    {
        $this->computedData = $computedData;
        return $this;
    }

    public function getRawData(): ?RawData
    {
        return $this->rawData;
    }

    public function setRawData(?RawData $rawData): self
    {
        $this->rawData = $rawData;

        return $this;
    }
}
