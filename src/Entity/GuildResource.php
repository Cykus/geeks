<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: 'App\Repository\GuildResourceRepository')]
class GuildResource implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups('json')]
    #[ORM\ManyToOne(targetEntity: 'App\Entity\Resource')]
    #[ORM\JoinColumn(nullable: false)]
    private $resource;

    #[ORM\ManyToOne(targetEntity: 'App\Entity\RawData', inversedBy: 'guildResources')]
    #[ORM\JoinColumn(nullable: false)]
    private $rawData;

    #[Groups('json')]
    #[ORM\Column(type: 'integer')]
    private $score;

    public function getId()
    {
        return $this->id;
    }

    public function __toString(): string
    {
        return (string)$this->getId();
    }

    public function getResource(): ?Resource
    {
        return $this->resource;
    }

    public function setResource(Resource $resource): self
    {
        $this->resource = $resource;

        return $this;
    }

    public function getRawData(): ?RawData
    {
        return $this->rawData;
    }

    public function setRawData(?RawData $rawData): self
    {
        $this->rawData = $rawData;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }
}
