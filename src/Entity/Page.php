<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class Page implements \Stringable
{

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    /**
     * @var string
     */
    #[Assert\NotBlank]
    #[ORM\Column(type: 'string', length: 255)]
    protected $name;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"name"})
     */
    #[ORM\Column(type: 'string', length: 255)]
    protected $slug;

    /**
     * @var string
     */
    #[ORM\Column(type: 'text', nullable: true)]
    protected $content;

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this->getName();
    }
    public function getId(): ?int
    {
        return $this->id;
    }
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }
    public function getName(): ?string
    {
        return $this->name;
    }
    public function setSlug(string $slug)
    {
        $this->slug = $slug;

        return $this;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setContent(string $content)
    {
        $this->content = $content;

        return $this;
    }
    public function getContent(): ?string
    {
        return $this->content;
    }

}