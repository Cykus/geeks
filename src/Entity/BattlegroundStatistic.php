<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: 'App\Repository\AdventureStatisticRepository')]
class BattlegroundStatistic
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: 'App\Entity\Player', inversedBy: 'battlegroundsStatistics')]
    #[ORM\JoinColumn(nullable: false)]
    private $player;

    #[ORM\ManyToOne(targetEntity: 'App\Entity\RawData', inversedBy: 'battlegroundsStatistics')]
    #[ORM\JoinColumn(nullable: false)]
    private $rawData;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $score;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $negotiations;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $battles;

    public function getId()
    {
        return $this->id;
    }

    public function getPlayer(): ?Player
    {
        return $this->player;
    }

    public function setPlayer(?Player $player): self
    {
        $this->player = $player;

        return $this;
    }

    public function getRawData(): ?RawData
    {
        return $this->rawData;
    }

    public function setRawData(?RawData $rawData): self
    {
        $this->rawData = $rawData;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(?int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getNegotiations(): ?int
    {
        return $this->negotiations;
    }

    public function setNegotiations(?int $negotiations): self
    {
        $this->negotiations = $negotiations;

        return $this;
    }

    public function getBattles(): ?int
    {
        return $this->battles;
    }

    public function setBattles(?int $battles): self
    {
        $this->battles = $battles;

        return $this;
    }
}
