<?php

namespace App\Entity;

use App\Handler\RawDataHandler;
use App\Repository\EraRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EraRepository::class)]
class Era implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 50)]
    private $slug;

    #[ORM\Column(type: 'integer')]
    private $number;

    #[ORM\Column(type: 'string', length: 12)]
    private $color;

    #[ORM\OneToMany(targetEntity: Player::class, mappedBy: 'destinationEra')]
    private $destinationPlayers;

    public function __construct()
    {
        $this->destinationPlayers = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string)$this->getNumber();
    }


    public function getRGBAColor(): ?string
    {
        return RawDataHandler::hex2rgba($this->getColor(),0.2);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return Collection|Player[]
     */
    public function getDestinationPlayers(): Collection
    {
        return $this->destinationPlayers;
    }

    public function addDestinationPlayer(Player $destinationPlayer): self
    {
        if (!$this->destinationPlayers->contains($destinationPlayer)) {
            $this->destinationPlayers[] = $destinationPlayer;
            $destinationPlayer->setDestinationEra($this);
        }

        return $this;
    }

    public function removeDestinationPlayer(Player $destinationPlayer): self
    {
        if ($this->destinationPlayers->removeElement($destinationPlayer)) {
            // set the owning side to null (unless already changed)
            if ($destinationPlayer->getDestinationEra() === $this) {
                $destinationPlayer->setDestinationEra(null);
            }
        }

        return $this;
    }
}
