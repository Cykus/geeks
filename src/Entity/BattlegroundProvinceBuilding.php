<?php

namespace App\Entity;

use App\Repository\BattlegroundProvinceBuildingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: BattlegroundProvinceBuildingRepository::class)]
class BattlegroundProvinceBuilding
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups('json')]
    #[ORM\ManyToOne(targetEntity: BattlegroundBuilding::class)]
    #[ORM\JoinColumn(nullable: false)]
    private $building;

    #[ORM\ManyToOne(targetEntity: BattlegroundProvince::class, inversedBy: 'buildings')]
    #[ORM\JoinColumn(nullable: false)]
    private $province;

    #[Groups('json')]
    #[ORM\OneToMany(targetEntity: BattlegroundProvinceBuildingCost::class, mappedBy: 'building', cascade: ['all'], orphanRemoval: true)]
    private $costs;

    #[Groups('json')]
    private array $computedData = [];

    public function __construct()
    {
        $this->costs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBuilding(): ?BattlegroundBuilding
    {
        return $this->building;
    }

    public function setBuilding(?BattlegroundBuilding $building): self
    {
        $this->building = $building;

        return $this;
    }

    public function getProvince(): ?BattlegroundProvince
    {
        return $this->province;
    }

    public function setProvince(?BattlegroundProvince $province): self
    {
        $this->province = $province;

        return $this;
    }

    /**
     * @return Collection|BattlegroundProvinceBuildingCost[]
     */
    public function getCosts(): Collection
    {
        return $this->costs;
    }

    public function addCost(BattlegroundProvinceBuildingCost $cost): self
    {
        if (!$this->costs->contains($cost)) {
            $this->costs[] = $cost;
            $cost->setBuilding($this);
        }

        return $this;
    }

    public function removeCost(BattlegroundProvinceBuildingCost $cost): self
    {
        if ($this->costs->removeElement($cost)) {
            // set the owning side to null (unless already changed)
            if ($cost->getBuilding() === $this) {
                $cost->setBuilding(null);
            }
        }

        return $this;
    }

    public function getComputedData(): array
    {
        return $this->computedData;
    }

    public function setComputedData(array $computedData): self
    {
        $this->computedData = $computedData;
        return $this;
    }
}
