<?php

namespace App\Entity;

use App\Handler\RawDataHandler;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: 'App\Repository\ResourceRepository')]
class Resource
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups('json')]
    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[Groups('json')]
    #[ORM\Column(type: 'string', length: 255)]
    private $slug;

    #[Groups('json')]
    #[ORM\ManyToOne(targetEntity: Era::class)]
    #[ORM\JoinColumn(nullable: false)]
    private $era;

    #[ORM\OneToMany(targetEntity: GuildTreasury::class, mappedBy: 'resource', orphanRemoval: true)]
    private $guildTreasuries;

    public function __construct()
    {
        $this->guildTreasuries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getEra(): ?Era
    {
        return $this->era;
    }

    public function setEra(?Era $era): self
    {
        $this->era = $era;

        return $this;
    }

    /**
     * @return Collection|GuildTreasury[]
     */
    public function getGuildTreasuries(): Collection
    {
        return $this->guildTreasuries;
    }

    public function addGuildTreasury(GuildTreasury $guildTreasury): self
    {
        if (!$this->guildTreasuries->contains($guildTreasury)) {
            $this->guildTreasuries[] = $guildTreasury;
            $guildTreasury->setResource($this);
        }

        return $this;
    }

    public function removeGuildTreasury(GuildTreasury $guildTreasury): self
    {
        if ($this->guildTreasuries->removeElement($guildTreasury)) {
            // set the owning side to null (unless already changed)
            if ($guildTreasury->getResource() === $this) {
                $guildTreasury->setResource(null);
            }
        }

        return $this;
    }
}
