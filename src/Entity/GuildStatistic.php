<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: 'App\Repository\GuildStatisticRepository')]
class GuildStatistic implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\OneToOne(targetEntity: 'App\Entity\RawData', inversedBy: 'guildStatistic', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private $rawData;

    /**
     * GuildStatistic constructor.
     * @param $score
     * @param $rank
     * @param $level
     * @param $prestige
     * @param $data
     */
    public function __construct(#[ORM\Column(type: 'integer')]
    private $score, #[ORM\Column(type: 'integer')]
    private $rank, #[ORM\Column(type: 'integer')]
    private $level, #[ORM\Column(type: 'integer')]
    private $prestige, #[ORM\Column(type: 'json')]
    private $data)
    {
    }

    public function getId()
    {
        return $this->id;
    }

    public function __toString(): string
    {
        return (string)$this->getId();
    }

    public function getRawData(): ?RawData
    {
        return $this->rawData;
    }

    public function setRawData(RawData $rawData): self
    {
        $this->rawData = $rawData;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getRank(): ?int
    {
        return $this->rank;
    }

    public function setRank(int $rank): self
    {
        $this->rank = $rank;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getPrestige(): ?int
    {
        return $this->prestige;
    }

    public function setPrestige(int $prestige): self
    {
        $this->prestige = $prestige;

        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data): self
    {
        $this->data = $data;

        return $this;
    }
}
