<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: 'App\Repository\PlayerRepository')]
class Player implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\OneToMany(targetEntity: 'App\Entity\Statistic', mappedBy: 'player', orphanRemoval: true)]
    private $statistics;

    #[ORM\OneToMany(targetEntity: 'App\Entity\AdventureStatistic', mappedBy: 'player', orphanRemoval: true)]
    private $adventuresStatistics;

    #[ORM\OneToMany(targetEntity: 'App\Entity\BattlegroundStatistic', mappedBy: 'player', orphanRemoval: true)]
    private $battlegroundsStatistics;

    #[ORM\Column(type: 'string', length: 7, nullable: true)]
    private $color;

    #[ORM\Column(type: 'integer')]
    private $rank;

    #[ORM\Column(type: 'boolean')]
    private $active;

    #[ORM\Column(type: 'json')]
    private $data;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $playerId;
    #[ORM\Column(type: 'datetime')]
    private $createdAt;

    #[ORM\Column(type: 'text', nullable: true)]
    private $comment;

    #[ORM\ManyToOne(targetEntity: Era::class)]
    private $era;

    #[ORM\OneToMany(targetEntity: GuildTreasury::class, mappedBy: 'player', orphanRemoval: true)]
    private $guildTreasuries;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $previousName;

    #[ORM\ManyToOne(targetEntity: Era::class, inversedBy: 'destinationPlayers')]
    private $destinationEra;

    #[ORM\OneToOne(targetEntity: User::class, mappedBy: 'player', cascade: ['persist', 'remove'])]
    private $user;

    public function __construct()
    {
        $this->statistics = new ArrayCollection();
        $this->adventuresStatistics = new ArrayCollection();
        $this->battlegroundsStatistics = new ArrayCollection();
        $this->guildTreasuries = new ArrayCollection();
        $this->rank = 9999;
        $this->active = true;
        $this->data = [];
        $this->createdAt = new \DateTime();
    }

    public function __toString(): string
    {
        return $this->getName().($this->getPreviousName()?' ('.$this->getPreviousName().')':'');
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Statistic[]
     */
    public function getStatistics(): Collection
    {
        return $this->statistics;
    }
    public function getLastBattleStatsDiff(): float
    {

        $stats =  $this->statistics->matching(Criteria::create()->orderBy(['id' => 'DESC'])->setMaxResults(2));
        if($stats->get(1) && $stats->get(1)->getRawData()->getCreatedAt() > new \DateTime('previous week monday')) {
            return $stats->get(0)->getBattles()- $stats->get(1)->getBattles();
        }
        return 0.0000001;
    }

    public function addStatistic(Statistic $statistic): self
    {
        if (!$this->statistics->contains($statistic)) {
            $this->statistics[] = $statistic;
            $statistic->setPlayer($this);
        }

        return $this;
    }

    public function removeStatistic(Statistic $statistic): self
    {
        if ($this->statistics->contains($statistic)) {
            $this->statistics->removeElement($statistic);
            // set the owning side to null (unless already changed)
            if ($statistic->getPlayer() === $this) {
                $statistic->setPlayer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AdventureStatistic[]
     */
    public function getAdventuresStatistics(): Collection
    {
        return $this->adventuresStatistics;
    }

    public function addAdventuresStatistic(Statistic $statistic): self
    {
        if (!$this->adventuresStatistics->contains($statistic)) {
            $this->adventuresStatistics[] = $statistic;
            $statistic->setPlayer($this);
        }

        return $this;
    }

    public function removeAdventureStatistic(Statistic $statistic): self
    {
        if ($this->adventuresStatistics->contains($statistic)) {
            $this->adventuresStatistics->removeElement($statistic);
            // set the owning side to null (unless already changed)
            if ($statistic->getPlayer() === $this) {
                $statistic->setPlayer(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|BattlegroundStatistic[]
     */
    public function getBattlegroundsStatistics(): Collection
    {
        return $this->battlegroundsStatistics;
    }

    public function addBattlegroundsStatistic(Statistic $statistic): self
    {
        if (!$this->battlegroundsStatistics->contains($statistic)) {
            $this->battlegroundsStatistics[] = $statistic;
            $statistic->setPlayer($this);
        }

        return $this;
    }

    public function removeBattlegroundStatistic(Statistic $statistic): self
    {
        if ($this->battlegroundsStatistics->contains($statistic)) {
            $this->battlegroundsStatistics->removeElement($statistic);
            // set the owning side to null (unless already changed)
            if ($statistic->getPlayer() === $this) {
                $statistic->setPlayer(null);
            }
        }

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getRank(): ?int
    {
        return $this->rank;
    }

    public function setRank(int $rank): self
    {
        $this->rank = $rank;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getData(): ?array
    {
        return $this->data;
    }

    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getPlayerId(): ?int
    {
        return $this->playerId;
    }

    public function setPlayerId(?int $playerId): self
    {
        $this->playerId = $playerId;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getEra(): ?Era
    {
        return $this->era;
    }

    public function setEra(?Era $era): self
    {
        $this->era = $era;

        return $this;
    }

    /**
     * @return Collection|GuildTreasury[]
     */
    public function getGuildTreasuries(): Collection
    {
        return $this->guildTreasuries;
    }

    public function addGuildTreasury(GuildTreasury $guildTreasury): self
    {
        if (!$this->guildTreasuries->contains($guildTreasury)) {
            $this->guildTreasuries[] = $guildTreasury;
            $guildTreasury->setPlayer($this);
        }

        return $this;
    }

    public function removeGuildTreasury(GuildTreasury $guildTreasury): self
    {
        if ($this->guildTreasuries->removeElement($guildTreasury)) {
            // set the owning side to null (unless already changed)
            if ($guildTreasury->getPlayer() === $this) {
                $guildTreasury->setPlayer(null);
            }
        }

        return $this;
    }

    public function getPreviousName(): ?string
    {
        return $this->previousName;
    }

    public function setPreviousName(?string $previousName): self
    {
        $this->previousName = $previousName;

        return $this;
    }

    public function getDestinationEra(): ?Era
    {
        return $this->destinationEra;
    }

    public function setDestinationEra(?Era $destinationEra): self
    {
        $this->destinationEra = $destinationEra;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        // unset the owning side of the relation if necessary
        if ($user === null && $this->user !== null) {
            $this->user->setPlayer(null);
        }

        // set the owning side of the relation if necessary
        if ($user !== null && $user->getPlayer() !== $this) {
            $user->setPlayer($this);
        }

        $this->user = $user;

        return $this;
    }
}
