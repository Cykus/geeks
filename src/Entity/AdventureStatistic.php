<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: 'App\Repository\AdventureStatisticRepository')]
class AdventureStatistic implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: 'App\Entity\Player', inversedBy: 'adventuresStatistics')]
    #[ORM\JoinColumn(nullable: false)]
    private $player;

    #[ORM\ManyToOne(targetEntity: 'App\Entity\RawData', inversedBy: 'adventuresStatistics')]
    #[ORM\JoinColumn(nullable: false)]
    private $rawData;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $score;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $conflicts;

    public function getId()
    {
        return $this->id;
    }

    public function __toString(): string
    {
        return (string)$this->getId();
    }

    public function getPlayer(): ?Player
    {
        return $this->player;
    }

    public function setPlayer(?Player $player): self
    {
        $this->player = $player;

        return $this;
    }

    public function getRawData(): ?RawData
    {
        return $this->rawData;
    }

    public function setRawData(?RawData $rawData): self
    {
        $this->rawData = $rawData;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(?int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getConflicts(): ?int
    {
        return $this->conflicts;
    }

    public function setConflicts(?int $conflicts): self
    {
        $this->conflicts = $conflicts;

        return $this;
    }
}
