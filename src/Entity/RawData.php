<?php

namespace App\Entity;

use App\Repository\RawDataRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RawDataRepository::class)]
class RawData
{
    final const TYPE_STATISTICS = 'statistics';
    final const TYPE_ADVENTURES = 'adventures';
    final const TYPE_BATTLEGROUNDS = 'battlegrounds';
    final const TYPE_RESOURCES= 'resources';
    final const TYPE_RESOURCES_LOGS= 'resources_logs';
    final const TYPE_CITY= 'city';
    final const TYPE_BATTLEGROUND = 'battleground';
    final const TYPE_BATTLEGROUND_PROVINCE = 'battleground_province';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $type;

    #[ORM\Column(type: 'boolean')]
    private bool $imported = false;

    #[ORM\Column(type: 'datetime')]
    private $createdAt;

    #[ORM\OneToMany(targetEntity: 'App\Entity\Statistic', mappedBy: 'rawData', orphanRemoval: true)]
    private $statistics;

    #[ORM\OneToMany(targetEntity: 'App\Entity\GuildResource', mappedBy: 'rawData', orphanRemoval: true)]
    private $guildResources;
    #[ORM\OneToOne(targetEntity: 'App\Entity\GuildStatistic', mappedBy: 'rawData', cascade: ['persist', 'remove'])]
    private $guildStatistic;

    #[ORM\OneToMany(targetEntity: 'App\Entity\AdventureStatistic', mappedBy: 'rawData', orphanRemoval: true)]
    private $adventuresStatistics;

    #[ORM\OneToMany(targetEntity: 'App\Entity\BattlegroundStatistic', mappedBy: 'rawData', orphanRemoval: true)]
    private $battlegroundsStatistics;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $filename;

    #[ORM\OneToMany(targetEntity: GuildTreasury::class, mappedBy: 'rawData', orphanRemoval: true)]
    private $guildTreasuries;

    #[ORM\Column(type: 'json')]
    private array $data = [];

    #[ORM\OneToOne(mappedBy: 'rawData', cascade: ['persist', 'remove'])]
    private ?Battleground $battleground = null;

    public function __construct()
    {
        $this->guildTreasuries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getData(): ?array
    {
        return $this->data;
    }

    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getImported(): ?bool
    {
        return $this->imported;
    }

    public function setImported(bool $imported): self
    {
        $this->imported = $imported;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(?string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }
    /**
     * @return Collection|Statistic[]
     */
    public function getStatistics(): Collection
    {
        return $this->statistics;
    }

    public function addStatistic(Statistic $statistic): self
    {
        if (!$this->statistics->contains($statistic)) {
            $this->statistics[] = $statistic;
            $statistic->setRawData($this);
        }

        return $this;
    }

    public function removeStatistic(Statistic $statistic): self
    {
        if ($this->statistics->contains($statistic)) {
            $this->statistics->removeElement($statistic);
            // set the owning side to null (unless already changed)
            if ($statistic->getRawData() === $this) {
                $statistic->setRawData(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|GuildResource[]
     */
    public function getGuildResources(): Collection
    {
        return $this->guildResources;
    }

    public function addGuildResource(GuildResource $guildResource): self
    {
        if (!$this->guildResources->contains($guildResource)) {
            $this->guildResources[] = $guildResource;
            $guildResource->setRawData($this);
        }

        return $this;
    }

    public function removeGuildResource(GuildResource $guildResource): self
    {
        if ($this->guildResources->contains($guildResource)) {
            $this->guildResources->removeElement($guildResource);
            // set the owning side to null (unless already changed)
            if ($guildResource->getRawData() === $this) {
                $guildResource->setRawData(null);
            }
        }

        return $this;
    }

    public function getGuildStatistic(): ?GuildStatistic
    {
        return $this->guildStatistic;
    }

    public function setGuildStatistic(GuildStatistic $guildStatistic): self
    {
        $this->guildStatistic = $guildStatistic;

        // set the owning side of the relation if necessary
        if ($this !== $guildStatistic->getRawData()) {
            $guildStatistic->setRawData($this);
        }

        return $this;
    }

    /**
     * @return Collection|AdventureStatistic[]
     */
    public function getAdventuresStatistics(): Collection
    {
        return $this->adventuresStatistics;
    }

    public function addAdventureStatistic(AdventureStatistic $statistic): self
    {
        if (!$this->adventuresStatistics->contains($statistic)) {
            $this->adventuresStatistics[] = $statistic;
            $statistic->setRawData($this);
        }

        return $this;
    }

    public function removeAdventureStatistic(AdventureStatistic $statistic): self
    {
        if ($this->adventuresStatistics->contains($statistic)) {
            $this->adventuresStatistics->removeElement($statistic);
            // set the owning side to null (unless already changed)
            if ($statistic->getRawData() === $this) {
                $statistic->setRawData(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|BattlegroundStatistic[]
     */
    public function getBattlegroundsStatistics(): Collection
    {
        return $this->battlegroundsStatistics;
    }

    public function addBattlegroundStatistic(BattlegroundStatistic $statistic): self
    {
        if (!$this->battlegroundsStatistics->contains($statistic)) {
            $this->battlegroundsStatistics[] = $statistic;
            $statistic->setRawData($this);
        }

        return $this;
    }

    public function removeBattlegroundStatistic(BattlegroundStatistic $statistic): self
    {
        if ($this->battlegroundsStatistics->contains($statistic)) {
            $this->battlegroundsStatistics->removeElement($statistic);
            // set the owning side to null (unless already changed)
            if ($statistic->getRawData() === $this) {
                $statistic->setRawData(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|GuildTreasury[]
     */
    public function getGuildTreasuries(): Collection
    {
        return $this->guildTreasuries;
    }

    public function addGuildTreasury(GuildTreasury $guildTreasury): self
    {
        if (!$this->guildTreasuries->contains($guildTreasury)) {
            $this->guildTreasuries[] = $guildTreasury;
            $guildTreasury->setRawData($this);
        }

        return $this;
    }

    public function removeGuildTreasury(GuildTreasury $guildTreasury): self
    {
        if ($this->guildTreasuries->removeElement($guildTreasury)) {
            // set the owning side to null (unless already changed)
            if ($guildTreasury->getRawData() === $this) {
                $guildTreasury->setRawData(null);
            }
        }

        return $this;
    }

    public function getBattleground(): ?Battleground
    {
        return $this->battleground;
    }

    public function setBattleground(?Battleground $battleground): self
    {
        // unset the owning side of the relation if necessary
        if ($battleground === null && $this->battleground !== null) {
            $this->battleground->setRawData(null);
        }

        // set the owning side of the relation if necessary
        if ($battleground !== null && $battleground->getRawData() !== $this) {
            $battleground->setRawData($this);
        }

        $this->battleground = $battleground;

        return $this;
    }
}
