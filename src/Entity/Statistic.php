<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: 'App\Repository\StatisticRepository')]
class Statistic implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: 'App\Entity\Player', inversedBy: 'statistics', cascade: ['persist'])]
    #[ORM\JoinColumn(nullable: false)]
    private $player;

    #[ORM\ManyToOne(targetEntity: 'App\Entity\RawData', inversedBy: 'statistics')]
    #[ORM\JoinColumn(nullable: false)]
    private $rawData;

    #[ORM\Column(type: 'integer')]
    private $score;

    #[ORM\Column(type: 'integer')]
    private int $battles = 0;

    #[ORM\ManyToOne(targetEntity: Era::class)]
    #[ORM\JoinColumn(nullable: false)]
    private $era;

    public function getId()
    {
        return $this->id;
    }

    public function __toString(): string
    {
        return (string)$this->getId();
    }

    public function getPlayer(): ?Player
    {
        return $this->player;
    }

    public function setPlayer(?Player $player): self
    {
        $this->player = $player;

        return $this;
    }

    public function getRawData(): ?RawData
    {
        return $this->rawData;
    }

    public function setRawData(?RawData $rawData): self
    {
        $this->rawData = $rawData;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getBattles(): ?int
    {
        return $this->battles;
    }

    public function setBattles(int $battles): self
    {
        $this->battles = $battles;

        return $this;
    }

    public function getEra(): ?Era
    {
        return $this->era;
    }

    public function setEra(?Era $era): self
    {
        $this->era = $era;

        return $this;
    }
}
