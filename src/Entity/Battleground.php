<?php

namespace App\Entity;

use App\Repository\BattlegroundRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: BattlegroundRepository::class)]
class Battleground
{
    #[Groups('json')]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups('json')]
    #[ORM\Column(type: 'datetime_immutable')]
    private $endsAt;

    #[Groups('json')]
    #[ORM\OneToMany(targetEntity: BattlegroundProvince::class, mappedBy: 'battleground', cascade: ['all'], orphanRemoval: true)]
    private $provinces;

    #[Groups('json')]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $mapId;

    #[Groups('json')]
    #[ORM\OneToMany(targetEntity: BattlegroundParticipant::class, mappedBy: 'battleground', cascade: ['all'], orphanRemoval: true)]
    private $participants;

    #[ORM\OneToOne(inversedBy: 'battleground', cascade: ['persist', 'remove'])]
    private ?RawData $rawData = null;

    public function __construct()
    {
        $this->provinces = new ArrayCollection();
        $this->participants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEndsAt(): ?\DateTimeImmutable
    {
        return $this->endsAt;
    }

    public function setEndsAt(\DateTimeImmutable $endsAt): self
    {
        $this->endsAt = $endsAt;

        return $this;
    }

    public function getStartsAt(): ?\DateTimeImmutable
    {
        return (clone $this->getEndsAt())->sub(new \DateInterval('P11D'));
    }

    /**
     * @return Collection|BattlegroundProvince[]
     */
    public function getProvinces(): Collection
    {
        return $this->provinces;
    }
    public function getProvinceById(?int $id): ?BattlegroundProvince
    {
        return $this->getProvinces()->filter(
            fn($entry) => $entry->getData()->getProvinceId() === $id
        )->first()?:null;
    }

    public function addProvince(BattlegroundProvince $province): self
    {
        if (!$this->provinces->contains($province)) {
            $this->provinces[] = $province;
            $province->setBattleground($this);
        }

        return $this;
    }

    public function removeProvince(BattlegroundProvince $province): self
    {
        if ($this->provinces->removeElement($province)) {
            // set the owning side to null (unless already changed)
            if ($province->getBattleground() === $this) {
                $province->setBattleground(null);
            }
        }

        return $this;
    }

    public function getMapId(): ?string
    {
        return $this->mapId;
    }

    public function setMapId(string $mapId): self
    {
        $this->mapId = $mapId;

        return $this;
    }

    /**
     * @return Collection|BattlegroundParticipant[]
     */
    public function getParticipants(): Collection
    {
        return $this->participants;
    }

    public function addParticipant(BattlegroundParticipant $participant): self
    {
        if (!$this->participants->contains($participant)) {
            $this->participants[] = $participant;
            $participant->setBattleground($this);
        }

        return $this;
    }

    public function removeParticipant(BattlegroundParticipant $participant): self
    {
        if ($this->participants->removeElement($participant)) {
            // set the owning side to null (unless already changed)
            if ($participant->getBattleground() === $this) {
                $participant->setBattleground(null);
            }
        }

        return $this;
    }

    public function getRawData(): ?RawData
    {
        return $this->rawData;
    }

    public function setRawData(?RawData $rawData): self
    {
        $this->rawData = $rawData;

        return $this;
    }
}
