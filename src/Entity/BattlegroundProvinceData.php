<?php

namespace App\Entity;

use App\Repository\BattlegroundProvinceDataRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: BattlegroundProvinceDataRepository::class)]
class BattlegroundProvinceData
{
    #[Groups('json')]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups('json')]
    #[ORM\Column(type: 'integer')]
    private $provinceId;

    #[Groups('json')]
    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[Groups('json')]
    #[ORM\Column(type: 'json')]
    private array $connections = [];

    #[Groups('json')]
    #[ORM\Column(type: 'string', length: 255)]
    private $short;

    #[Groups('json')]
    #[ORM\Column(type: 'json')]
    private array $flag = [];

    #[ORM\Column(type: 'string', length: 255)]
    private $mapId;

    #[ORM\OneToMany(targetEntity: BattlegroundProvince::class, mappedBy: 'data', orphanRemoval: true)]
    private $provinces;

    public function __construct()
    {
        $this->provinces = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProvinceId(): ?int
    {
        return $this->provinceId;
    }

    public function setProvinceId(int $provinceId): self
    {
        $this->provinceId = $provinceId;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getConnections(): ?array
    {
        return $this->connections;
    }

    public function setConnections(array $connections): self
    {
        $this->connections = $connections;

        return $this;
    }

    public function getShort(): ?string
    {
        return $this->short;
    }

    public function setShort(string $short): self
    {
        $this->short = $short;

        return $this;
    }

    public function getFlag(): ?array
    {
        return $this->flag;
    }

    public function setFlag(array $flag): self
    {
        $this->flag = $flag;

        return $this;
    }

    public function getMapId(): ?string
    {
        return $this->mapId;
    }

    public function setMapId(string $mapId): self
    {
        $this->mapId = $mapId;

        return $this;
    }

    /**
     * @return Collection|BattlegroundProvince[]
     */
    public function getProvinces(): Collection
    {
        return $this->provinces;
    }

    public function addProvince(BattlegroundProvince $province): self
    {
        if (!$this->provinces->contains($province)) {
            $this->provinces[] = $province;
            $province->setData($this);
        }

        return $this;
    }

    public function removeProvince(BattlegroundProvince $province): self
    {
        if ($this->provinces->removeElement($province)) {
            // set the owning side to null (unless already changed)
            if ($province->getData() === $this) {
                $province->setData(null);
            }
        }

        return $this;
    }
}
