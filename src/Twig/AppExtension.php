<?php

namespace App\Twig;

use App\Entity\Item;
use App\Entity\RawData;
use App\Entity\Warning;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{

    private $dir;
    public function __construct()
    {
        $this->dir = __DIR__.'/../../public/images/item/';
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('color_inverse', $this->color_inverse(...)),
        ];
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('format_value', $this->format_value(...)),
            new TwigFilter('format_guild_value', $this->format_guild_value(...)),
            new TwigFilter('ban_date', $this->ban_date(...)),
            new TwigFilter('sum', 'array_sum'),
        ];
    }

    public function color_inverse($color)
    {
        $color = str_replace('#', '', (string) $color);
        if (strlen($color) != 6){ return '000000'; }
        $rgb = '';
        for ($x=0;$x<3;$x++){
            $c = 255 - hexdec(substr($color,(2*$x),2));
            $c = ($c < 0) ? 0 : dechex($c);
            $rgb .= (strlen($c) < 2) ? '0'.$c : $c;
        }
        return '#'.$rgb;
    }

    public function format_value($value, $index, array $values, $type, $dates = null, $last = false, $onlyPoints = false)
    {
        $append = '';
        if($type == 'battleground') {
            $orgValue = $value['score'];
            echo $orgValue;
            if($orgValue === null) {
                $text = '--';
            }
            else {
                $text = number_format($orgValue, 0,',', ' ');
            }
            $date = new \DateTime();
            $date->setTimestamp($dates[$index]);
            if($onlyPoints) {
                if($value['score'] === null) {
                    return 0;
                }
                return $value['score'];
            }
            if($value['score'] === null) {
                $text =' <span class="badge badge-secondary">'.$text.$append.'</span>';
            }
            else {
                if($value['score'] >= 30) {
                    $badge = 'success';
                }
                elseif($value['score'] >= 1) {
                    $badge = 'warning';
                }
                else {
                    $badge = 'danger';
                }
                $text =' <span class="badge badge-'.$badge.'">Negocjacje: '.number_format($value['negotiations'], 0,',', ' ').
                    ', Walki: '.number_format($value['battles'], 0,',', ' ').'</span>';
            }
            return $text;
        }
        $orgValue = $value;
        if($index === null || $last === true ) {
//            $append = "&#216;";
        }
        $max = false;
        if(is_array($value)) {
            if($index == null) {
                $index = count($value)-1;
            }
            $tmp = array_values(array_filter( $value, 'strlen' ));
            if(count($tmp)) {
                if($type == 'statistic') {
                    if(count($tmp) > 1) {
                        $value = (int)(($tmp[count($tmp)-1]-$tmp[0]) / (count($tmp)-1));
                    }
                    else {
                        $value = null;
                    }
                }
                else {

                    $value = (int)(array_sum($tmp) / count($tmp));
                }
            }
            else {
                $value = 0;
            }
        }
        if($value === null) {
            $text = '--';
        }
        else {
            $text = number_format($value, 0,',', ' ');
        }
        if($type == 'statistic') {
            if($index>0) {
                if($last) {
                    if($onlyPoints) {
                        return $value;
                    }
                    else {
                        return $text;
                    }
                }
                $prev_value = $value-$values[$index-1];
                $formated_value = number_format($prev_value, 0,',', ' ');
                if($value === null) {

                }
                if($onlyPoints) {
                    return $prev_value;
                }
                elseif($prev_value>0 && $values[$index-1]) {
                    if($prev_value<=1212) {
                        $text.=' <span class="badge badge-warning">(+'.$formated_value.')</span>';
                    }
                    else {
                        $text.=' <span class="badge badge-success">(+'.$formated_value.')</span>';
                    }
                }
                elseif($values[$index-1] !== null && $values[$index-1] >= $value) {
                    $text.=' <span class="badge badge-danger">('.$formated_value.')</span>';
                }
            }
        }
        elseif($type == 'resource') {
            if($index>0) {
                $prev_value = $value-$values[$index-1];
                $formated_value = number_format($prev_value, 0,',', ' ');
                if($value === null) {

                }
                if($onlyPoints) {
                    return $formated_value;
                }
                elseif($prev_value>0 && $values[$index-1]) {
                    $text.=' <span class="badge badge-success">(+'.$formated_value.')</span>';
                }
                elseif($values[$index-1] !== null && $values[$index-1] >= $value) {
                    $text.=' <span class="badge badge-danger">('.$formated_value.')</span>';
                }
            }
        }
        elseif($type == 'adventure') {
            $date = new \DateTime();
            $date->setTimestamp($dates[$index]);
            if($last) {
                $append .= ' (';
                $append .= round($value/48*100,2);
                $append .= '%)';
            }
            $min = 48;
            if($onlyPoints) {
                if($value === null) {
                    return 0;
                }
                return $text;
            }
            if($value === null) {
                $text =' <span class="badge badge-secondary">'.$text.$append.'</span>';
            }
            elseif($value<$min) {
                $text =' <span class="badge badge-danger">'.$text.$append.'</span>';
            }
            else {
                if($text == 64) {
                    $max = true;
                }
                $text =' <span class="badge badge-success" style="'.($max?'color:#0027c5;':'').'">'.$text.$append.'</span>';
            }
        }
        return $text;
    }

    public function format_guild_value(RawData $value, $index, $key, array $values, $onlyPoints = false)
    {
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $value = $propertyAccessor->getValue($value->getGuildStatistic(), $key);
        $text = number_format($value);
        if($index>0) {
            $prev_value = $value-$propertyAccessor->getValue($values[$index-1]->getGuildStatistic(), $key);
            $formated_value = number_format($prev_value);
            if($onlyPoints) {
                return $text;
            }
            if($key == 'rank') {
                if($prev_value == 0) {
                    $text.=' <span class="badge badge-warning">('.$formated_value.')</span>';
                }
                elseif($prev_value<0) {
                    $text.=' <span class="badge badge-success">('.$formated_value.')</span>';
                }
                else {
                    $text.=' <span class="badge badge-danger">(+'.$formated_value.')</span>';
                }
            }
            else {
                if($prev_value == 0) {
                    $text.=' <span class="badge badge-warning">('.$formated_value.')</span>';
                }
                elseif($prev_value<0) {
                    $text.=' <span class="badge badge-danger">('.$formated_value.')</span>';
                }
                else {
                    $text.=' <span class="badge badge-success">(+'.$formated_value.')</span>';
                }
            }
        }
        return $text;
    }

    public function ban_date(Collection $warnings)
    {
        $c = count($warnings);
        /** @var Warning $lastWarning */
        $lastWarning = $warnings->first();
        /** @var \DateTime $date */
        $date = clone $lastWarning->getDate();
        if($c == 1) {
            $date->add(new \DateInterval('P1W'));
        }
        elseif($c == 2) {
            $date->add(new \DateInterval('P2W'));
        }
        elseif($c == 3) {
            $date->add(new \DateInterval('P4W'));
        }
        elseif($c > 3) {
            return false;
        }
        return $date;
    }
}