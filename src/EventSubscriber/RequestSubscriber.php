<?php

namespace App\EventSubscriber;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;

class RequestSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly Security $security, private readonly EntityManagerInterface $em)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            RequestEvent::class => 'onKernelRequest',
            KernelEvents::CONTROLLER => 'onCoreController',
        ];
    }

    public function onKernelRequest(RequestEvent $event)
    {

        try {
            $request = $event->getRequest();

            if(str_starts_with($request->getPathInfo(), '/_')) {
                return;
            }
            if(str_starts_with($request->getPathInfo(), '/custom_view')) {
                return;
            }

            if($request->getClientIp() === '93.105.223.55') {
                return;
            }

            $query = "INSERT INTO stat (`url`, `ip`, `date`, `time`, `amount`) values(?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE amount = amount+1, `time` = ?";

            $this->em->getConnection()->executeQuery($query, [
                $request->getUri(),
                $request->getClientIp(),
                (new \DateTime())->format('Y-m-d'),
                (new \DateTime())->format('H:i:s'),
                1,
                (new \DateTime())->format('H:i:s'),
            ]);
        }
        catch (\Exception) {
        }
    }
    public function onCoreController(ControllerEvent $event)
    {
        if($event->getRequestType() == HttpKernelInterface::MAIN_REQUEST)
        {
            $user = $this->security->getUser();
            $delay = new \DateTime("2 minutes ago");

            if($user instanceof User && $user->getLastActivity() < $delay)
            {
                $user->setLastActivity(new \DateTime());
                $this->em->flush($user);
            }
        }
    }
}
