<?php

namespace App\Command;

use App\Handler\RawDataHandler;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:import-left')]
class ImportLeftCommand extends Command
{

    public function __construct(private readonly RawDataHandler $rawDataHandler)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->rawDataHandler->import();
        return Command::SUCCESS;
    }
}
