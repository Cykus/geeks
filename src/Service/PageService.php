<?php
/**
 * Created by PhpStorm.
 * User: cykus
 * Date: 01.02.19
 * Time: 19:34
 */

namespace App\Service;


use App\Entity\Page;
use App\Entity\RawData;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

class PageService
{
    /**
     * PageService constructor.
     * @param $em
     */
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function getPages(){

        /** @var QueryBuilder $qb */
        $qb =$this->em->getRepository(Page::class)->createQueryBuilder('e');
        return $qb->select('e.name, e.slug')->getQuery()->getArrayResult();
    }

    public function getLastUpdate(): \DateTime {
        return $this->em->getRepository(RawData::class)->lastUpdate();
    }
}